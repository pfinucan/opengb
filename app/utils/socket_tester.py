# Echo client program
import socket
import sys, time, msvcrt

HOST = 'localhost'    # The remote host
PORT = 7777              # The same port as used by the server
TIMEOUT = 5 # number of seconds your want for timeout

def read_socketlines(sock, recv_buffer=4096, delim='\n'):
    buffer = ''
    data = True
    while data:
            data = sock.recv(recv_buffer)
            buffer += data

            while buffer.find(delim) != -1:
                    line, buffer = buffer.split('\n', 1)
                    yield line
    return

def readInput( caption, default, timeout = 5):
    start_time = time.time()
    sys.stdout.write('%s(%s):'%(caption, default));
    input = ''
    while True:
        if msvcrt.kbhit():
            chr = msvcrt.getche()
            if ord(chr) == 13: # enter_key
                break
            elif ord(chr) >= 32: #space_char
                input += chr
        if len(input) == 0 and (time.time() - start_time) > timeout:
            break

    print ''  # needed to move to next line
    if len(input) > 0:
        return input
    else:
        return default

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
time.sleep(.5)
s.sendall( 'M105\r\n' )
time.sleep(.5)
"""loop forever and copy socket->serial"""
while 1:
    try:
        for data in read_socketlines(s):
            print "<---- socket read ---->"
            print data
            if not data:
                break
            print "<------------------> " + str(data)
            time.sleep(3)
            print "sending M105"
            s.sendall( 'M105\r\n')
        if not data:
            break
        print data
        time.sleep(3)
        print "sending M105"
        s.sendall( 'M105\r\n')
    except socket.error, msg:
        # probably got disconnected
        print msg
        break

        
#data = s.recv(1024)
s.close()
print 'Closed'



