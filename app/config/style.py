style = {}
style['name'] = 'touch'
style['colors'] = {
            'gray':           '#%02x%02x%02x' % (240, 240, 240),
            'dark_gray':      '#%02x%02x%02x' % (199, 200, 202),
            'yellow':         '#%02x%02x%02x' % (250, 220, 10),                
}
style['fonts'] = {
            'large font':       {'family':'Helvetica', 
                                'size': 18},
            'AWF':              {'family':'Proxima Nova Th', 
                                 'size':18},
            'AWF_large':        {'family':'Proxima Nova Th', 
                                 'size':36},
            'AWF_logo':         {'family':'FontAwesome', 
                                 'size':42},
            'AWF_logo_small':   {'family':'FontAwesome', 
                                 'size':14},
            'AWF_headings':     {'family':'Proxima Nova Th', 
                                 'size':22},
            'AWF_small':        {'family':'Proxima Nova Th', 
                                 'size':14},
}
style['styles'] = {
# Labels #
            'TLabel': {         
                'font' : 'AWF', 
                'background':'black', 
                'foreground':'white',
                },
            'small.TLabel': {         
                'font' : 'AWF_small', 
                'background':'black', 
                'foreground':'white',
                },
            'large.TLabel': {         
                'font' : 'AWF_large', 
                'background':'black', 
                'foreground':'white',
                },
            'default.TLabel':  { 
                'font' : 'AWF', 
                'background':'black', 
                'foreground':'white',
                },
            'logo.TLabel':  { 
                'font' : 'AWF_logo', 
                },
            'small.logo.TLabel':  { 
                'font' : 'AWF_logo_small', 
                },
            'active.logo.TLabel':  { 
                'background':'yellow',
                'foreground':'black',
                },
            'active.small.logo.TLabel':  { 
                'font' : 'AWF_logo_small', 
                'background':'yellow',
                'foreground':'black',
                },
            'active.TLabel':  { 
                'background':'yellow', 
                'foreground':'black',
                },
            'active.small.TLabel':  { 
                'font' : 'AWF_small', 
                'background':'yellow', 
                'foreground':'black',
                },
            'active.large.TLabel':  { 
                'font' : 'AWF_large', 
                'background':'yellow', 
                'foreground':'black',
                },
            'white.TLabel':  {  
                'background':'white', 
                'foreground':'black',
                },
            'gray.TLabel':  {  
                'background':'gray', 
                },
            'yellow.TLabel':  { 
                'foreground':'yellow',
                },
            'files.TLabel':  { 
                'font' : 'AWF_small', 
                'background':'white', 
                'foreground':'black',
                },
            'blackOngray.TLabel':  {  
                'background':'gray', 
                'foreground':'black',
                },
            'small.blackOngray.TLabel':  {  
                'font' : 'AWF_small',
                },
            'blackOngray.logo.TLabel':  {  
                'background':'gray', 
                'foreground':'black',
                },
            'large.blackOngray.TLabel':  {  
                'font' : 'AWF_large', 
                },
# Frames #    
            'TFrame': {         
                'background':'black', 
                },
            'default.TFrame':  { 
                'font' : 'AWF', 
                'background':'black', 
                'foreground':'white',
                },
            'active.TFrame':  { 
                'background':'yellow', 
                },
            'white.TFrame':  {  
                'background':'white', 
                },
            'gray.TFrame':  {  
                'background':'gray', 
                },
            'darkgray.TFrame':  {  
                'background':'dark_gray', 
                },
            'DarkGray.TFrame':  {  
                'background':'DarkGray', 
                },
            'files.TFrame':  { 
                'background':'white', 
                },
            'blackOngray.TFrame':  {  
                'background':'gray', 
                },
            'test.TFrame':  {  
                'background':'blue', 
                },
# Buttons #
            'TFrame': {         
                'background':'black', 
                },
}
icons = {
        'PRINT':    unichr(61875),
        'STATUS':   unichr(61668),
        'TOOLS':    unichr(61613),
        'SETTINGS': unichr(61614),
        'CLOUD':    unichr(61677),
        'USB':      unichr(61587),
        
        'play':     unichr(61515),
        'pause':    unichr(61516),
        'flame':    unichr(61549),
        'degree':   unichr(176),
        'nav_back': unichr(61523),
        'nav_up':   unichr(61559),
        'nav_fwd':  unichr(61524),
        'off':      unichr(61956),
        'on':       unichr(61957),
        'x':        unichr(61453),

         
        }
sizes = {'column_left': 300,
         'column_right': 500,
         'column_width_small': 100,
         'column_width_large': 700,
         'screen_height': 480,
         'screen_width': 800,
         'info_height': 60,
         'page_height': 420,
         
         }
         

                                 
                                 
                                 
                                 
                                 
                                 