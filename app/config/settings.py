# Settings file
import os

PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
STATIC_PATH = PATH + "/server/static/images/"

BAUDRATE = 115200
DEVICE = "/dev/ttyACM0"