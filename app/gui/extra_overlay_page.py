import time
import Tkinter as tk
import ttk, tkFont
from resources import Frame, Label, FixedFrame, LogoButton
import logging

logger = logging.getLogger(__name__)

class ExtraOverlayPage(ttk.Frame):
    def __init__(self, parent, *args, **kwargs):
        self.logger =  logger
        
        self.parent = parent
        ttk.Frame.__init__(self, parent, *args, width=700, height=480, style='active.TFrame', **kwargs)
        self.grid_propagate(0)
        
        self.deployed = False
        self.drag = {'ing':False, 'start':0, 'end':0, 'x':0, 'y':0, 'x1':0,'y1':0}
        self.deployed_x = 300
        self.speed = 50
        self.edge = self.parent.sizes['screen_width'] 
        self.target = self.edge
        self.tab_offset = 44
        
        labels = ['Top Label', 'Middle Label', 'Bottom Label']
        self.panels = []
        
        for idx, label in enumerate(labels):
            temp = []
            temp.append(Frame(self, style='DarkGray.TFrame', padding=(2,2), width=700, height=60 ))
            temp[-1].grid(row=idx, column=0, sticky='W')         
            temp.append(Frame(temp[-1], style='TFrame', padding=(2,2), width=700, height=60 ))
            temp[-1].bind("<1>", self.togle)  
            temp[-1].grid(row=0, sticky='NEW', padx=0, pady=0)
            temp.append(Label(temp[-1], text=label, style="TLabel"))
            temp[-1].grid(row=0, sticky='NEW', padx=5, pady=5)
            temp.append(Frame(temp[0], style='TFrame', width=700, height=300 ))
            self.panels.append(temp)
            
        self.panels[0][0].config(height=360)
        self.logger.debug('Widget style: %s', self.panels[0][0].winfo_class())
            
        
#       self.top_out = Frame(self, style='DarkGray.TFrame', padding=(2,2), width=700, height=360 )
#       self.top_inside = Frame(self.top_out, style='TFrame', padding=(2,2), width=700, height=60 )
#       self.top_label = Label(self.top_inside, text='Top Label', style="TLabel")
#       self.top_content = Frame(self.top_out, style='TFrame', width=700, height=300 )
#       
#       self.top_label.grid(row=0, column=0, sticky='W') 
#       self.top_out.grid(row=0, sticky='NEW', padx=0, pady=0)
#       self.top_inside.grid(row=0, sticky='NEW', padx=5, pady=5)
#       #self.top_content.grid(row=1, sticky='NEW', padx=5, pady=5)
#       
#       self.middle_out = Frame(self, style='DarkGray.TFrame', padding=(2,2), width=700, height=60 )
#       self.middle_inside = Frame(self.middle_out, style='TFrame', padding=(2,2), width=700, height=60 )
#       self.middle_label = Label(self.middle_inside, text='Middle Label', style="TLabel")
#       self.middle_content = Frame(self.middle_out, style='TFrame', width=700, height=300 )
#       self.middle_label.grid(row=0, column=0, sticky='W')
#       self.middle_out.grid(row=1, sticky='NEW', padx=0, pady=0)
#       self.middle_inside.grid(sticky='NEW', padx=5, pady=5)
#       #self.middle_content.grid(row=1, sticky='NEW', padx=5, pady=5)
#       
#       self.bottom_out = Frame(self, style='DarkGray.TFrame', padding=(2,2), width=700, height=60 )
#       self.bottom_inside = Frame(self.bottom_out, style='TFrame', padding=(2,2), width=700, height=60 )
#       self.bottom_label = Label(self.bottom_inside, text='Bottom Label', style="TLabel")
#       self.bottom_content = Frame(self.middle_out, style='TFrame', width=700, height=300 )
#       self.bottom_label.grid(row=0, column=0, sticky='W')
#       self.bottom_out.grid(row=2, sticky='NEW', padx=0, pady=0)
#       self.bottom_inside.grid(sticky='NEW', padx=5, pady=5)
#       #self.bottom_content.grid(row=1, sticky='NEW', padx=5, pady=5)
        
#        self.top_inside.bind("<1>", self.togle)
#        self.middle_inside.bind("<1>", self.togle)
#        self.bottom_inside.bind("<1>", self.togle)
        
        #self.panels = [self.top_out, self.middle_out, self.bottom_out]
        
        #self.test_label = Label(self.top_out, text='test label', style="TLabel")
        #self.test_label.grid(row=1, column=0, sticky='W')
        

        # Swipe test 
        self.place(x=50, y=0) 
        #self.parent.bind_all("<ButtonRelease-1>", self.onSwipeEnd)
        #self.parent.bind_all('<B1-Motion>', self.onSwipe)
        
        
       # ################ Start of old code ################################################
        self.buttons = []
        self.XY_frame = tk.LabelFrame(self.panels[0][3], text="X Y Control")
        self.XY_frame.grid(row=1, columnspan=7, sticky='W', padx=5, pady=5, ipadx=5, ipady=5)
        
        tk.Button(self.XY_frame, text='+Y', width=4, height=3, command=lambda: self.bump("Y")).grid(row=1, column=2, sticky='WE')
        tk.Button(self.XY_frame, text='-X', width=4, height=3, command=lambda: self.bump("X-")).grid(row=2, column=1, sticky='WE')
        tk.Button(self.XY_frame, text='+X', width=4, height=3, command=lambda: self.bump("X")).grid(row=2, column=3, sticky='WE')
        tk.Button(self.XY_frame, text='-Y', width=4, height=3, command=lambda: self.bump("Y-")).grid(row=3, column=2, sticky='WE')
        
        self.Z_frame = tk.LabelFrame(self.panels[0][3], text="Z Control")
        self.Z_frame.grid(row=1, column=8, columnspan=1, sticky='W', padx=5, pady=5, ipadx=5, ipady=5)
        tk.Button(self.Z_frame, text='+Z', width=4, height=3, command=lambda: self.bump("Z")).grid(row=1, column=1, sticky='WE')
        tk.Button(self.Z_frame, text='-Z', width=4, height=3, command=lambda: self.bump("Z-")).grid(row=2, column=1, sticky='WE')

        self.step_size = tk.LabelFrame(self.panels[0][3], text="Step Size")
        self.step_size.grid(row=1, column=9, columnspan=1, sticky='W', padx=5, pady=5, ipadx=5, ipady=5)
        
        self.bump_sizes = ["0.1", "1", "10"]
        for n, size in enumerate(self.bump_sizes):
            self.logger.debug('Movement bump size: %s, %s', size, n)
            button = tk.Button(self.step_size, text=size, width=4, height=3, 
                          command=lambda n=n, size=size: self.set_bump(n, size))
            button.grid(row=n+1, column=1, sticky='WE')
            self.buttons.append(button)
            
        self.heat_frame = tk.LabelFrame(self.panels[1][3], text="Preheat Settings")
        self.heat_frame.grid(row=1, column=10, columnspan=1, sticky='W', padx=5, pady=5, ipadx=5, ipady=5)   
        tk.Button(self.heat_frame, text='Preheat 200C', width=14, height=2, command=lambda: self.send_gcode("M104 S200")).grid(row=1, column=1, sticky='WE')
        tk.Button(self.heat_frame, text='Heat Off', width=14, height=2, command=lambda: self.send_gcode("M104 S0")).grid(row=3, column=1, sticky='WE')
        tk.Button(self.heat_frame, text='Motors Off', width=14, height=2, command=lambda: self.send_gcode("M18")).grid(row=4, column=1, sticky='WE')
        tk.Button(self.heat_frame, text='Fan ON', width=14, height=2, command=lambda: self.send_gcode("M106")).grid(row=5, column=1, sticky='WE')
        tk.Button(self.heat_frame, text='Fan off', width=14, height=2, command=lambda: self.send_gcode("M107")).grid(row=6, column=1, sticky='WE')

        self.home_frame = tk.LabelFrame(self.panels[2][3], text="Homing")
        self.home_frame.grid(row=2, column=1, columnspan=10, sticky='WE', padx=5, pady=5, ipadx=5, ipady=5)   
        tk.Button(self.home_frame, text='Home X', width=5, height=2, command=lambda: self.send_gcode("G28 X")).grid(row=1, column=4, sticky='WE')
        tk.Button(self.home_frame, text='Home Y', width=5, height=2, command=lambda: self.send_gcode("G28 Y")).grid(row=1, column=3, sticky='WE')
        tk.Button(self.home_frame, text='Home Z', width=5, height=2, command=lambda: self.send_gcode("G28 Z")).grid(row=1, column=2, sticky='WE')
        tk.Button(self.home_frame, text='Home All', width=5, height=2, command=lambda: self.send_gcode("G28")).grid(row=1, column=1, sticky='WE')

        tk.Button(self.home_frame, text='Delete Current', width=5, height=2, command=self.delete_file).grid(row=4, column=1, sticky='WE')
        
        #print len(self.size_bns)
        self.set_bump(2,"10")
    
    def delete_file(self):
        file_id = self.parent.print_run_page.file_info['id']
        self.logger.debug('file to delete: %s', file_id)
        self.parent.dispatcher.delete_print_file(id=file_id)

    def bump(self, part_gcode):
        gcode = "G1 " + part_gcode + self.bump_size
        print "bump", gcode
        self.parent.dispatcher.write_gcode_priorty("G91")
        self.parent.dispatcher.write_gcode_priorty(gcode)  
        self.parent.dispatcher.write_gcode_priorty("G90")
        
    def set_bump(self, n, size):
        self.logger.debug('Set bump, index size: %s, %s', n, size)

        self.bump_size = size
        for index, button in enumerate(self.buttons):
            if index == n:
                button.config(state="disabled")
            elif button['state'] == "disabled":
                button.config(state="active")
                
    def send_gcode(self, gcode):
        self.logger.debug("send gcode: %s", gcode)
        self.parent.dispatcher.write_gcode_priorty(gcode)
        
        
    # ################ End of old code ################################################

    def togle(self, e):
        self.logger.debug('Overlay toggle: %s', e)
        self.logger.debug('Widget style: %s', e.widget.winfo_class())
        if e.widget.winfo_class() == "TLabel":
            outer = e.widget.master.master
        elif e.widget.winfo_class() == "TFrame":
            outer = e.widget.master
        
        #outer.config(height=
        
        for panel in self.panels:
            self.logger.debug('Panel Height: %s', panel[0].winfo_height())
            if panel[0] == outer:
                self.logger.debug('Matched widget')
                panel[0].config(height=360)
                panel[3].grid(row=1, sticky='NEW', padx=5, pady=5)

            else:
                panel[0].config(height=60)
                panel[3].grid_remove()
        
        

        
