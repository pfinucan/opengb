import Tkinter as tk
import ttk, tkFont
from gui.resources import Frame, Label, FixedFrame, LogoButton

class SettingsPage(Frame):
    def __init__(self, parent, *args, **kwargs):
        kwargs['style'] = 'active.TFrame'
        Frame.__init__(self, parent.fr_right, *args, name="settings", **kwargs)
        self.parent = parent
        
        
        

    def page(self, e):
        
        self.parent.empty_page(self.parent.fr_right)
        self.grid()

        # update nav
        self.parent.var_home_logo.set(unichr(61523))       
        self.parent.var_home.set("SETTINGS")
        self.parent.menu_page.home.configure(style="yellow.TLabel")

    def unpage(self):
        self.parent.empty_page(self.parent.fr_right)
 
    def server(self, event):
        print "server button"
        if  hasattr(self.parent, 'dispatcher'):
            self.parent.dispatcher.startserver()