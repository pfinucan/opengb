import config.settings
import Tkinter as tk
import ttk, tkFont
from gui.resources import Frame, Label, FixedFrame, LogoButton
import logging

logger = logging.getLogger(__name__)

class Keyboard(Frame):
    def __init__(self, parent, *args, **kwargs):
        self.logger = logger
        self.logger.info("keyboard logger name: %s", __name__)
        
        self.parent = parent
        
        Frame.__init__(self, parent, *args, width=800, height=350, style='active.TFrame', **kwargs)
        self.lower_case = True
        
        lines = []
        lines.append(['~','@','#','$','%','^','&','*','(',')','-','+'])
        lines.append(['1','2','3','4','5','6','7','8','9','0'])
        lines.append(['q','w','e','r','t','y','u','i','o','p'])
        lines.append(['a','s','d','f','g','h','j','k','l'])
        lines.append(['z','x','c','v','b','n','m'])
        
        self.entry_frame = Frame(self, padding=(1), style='active.TFrame' )
        self.entry_text = Label(self.entry_frame, text="", width=35, padding=(10), style='white.TLabel')
        self.entry_text.grid(row=0, column=0, sticky='NSEW', padx=5, pady=5)

        self.delete = Label(self.entry_frame, 
                            text=self.parent.icons['nav_back'], 
                            padding=(10), 
                            style='logo.TLabel'
                            )
        self.delete.grid(row=0, column=1, sticky='NSEW', padx=5, pady=5)
        self.delete.bind("<Button-1>", self.delete_buton_pushed)
        
        self.done = Label(self.entry_frame, text='Done', padding=(10), style='TLabel')
        self.done.grid(row=0, column=2, sticky='NSEW', padx=5, pady=5)
        self.done.bind("<Button-1>", self.finished_with_keyboard)
        
        self.entry_frame.grid(columnspan=3, row=0, column=0, sticky='NSEW', padx=5, pady=5)
        
        
        self.line_frames = []
        self.buttons = []
        
        for idx, line in enumerate(lines, start=1):
            self.line_frames.append(Frame(self, padding=(1), style='gray.TFrame' ))
            for button_idx, button in enumerate(line, start=1):
                self.buttons.append(Label(self.line_frames[-1], text=button, padding=(15,5)))
                self.buttons[-1].grid(row=0, column=button_idx, padx=4, pady=4)
                self.buttons[-1].bind("<Button-1>", self.button_pushed)
            self.line_frames[-1].grid(row=idx, column=1, sticky='E')

        self.shift = Label(self.line_frames[-1], 
                            text=self.parent.icons['nav_up'], 
                            padding=(10), 
                            style='active.small.logo.TLabel'
                            )
        self.shift.grid(row=0, column=0, sticky='NSEW', padx=5, pady=5)
        self.shift.bind("<Button-1>", self.toggle_case)
        
        #self.place(x=0, y=130)
        
    def button_pushed(self, event):
        self.logger.debug("key pushed: %s", event)
        self.logger.debug("widget: %s", event.widget.text)
        #self.logger.debug("widget dir: %s", dir(event.widget))
        self.entry_text.text = self.entry_text.text + event.widget.text
        
    def delete_buton_pushed(self, event):
        self.logger.debug("Delete pushed: %s", event)
        self.entry_text.text  = self.entry_text.text [:-1]
        
    def toggle_case(self, event):
        self.logger.debug("Shift: %s", event)
        if self.lower_case:
            self.lower_case = False
            for button in self.buttons:
                button.text = button.text.upper()
        else:
            self.lower_case = True
            for button in self.buttons:
                button.text = button.text.lower()
                
    def finished_with_keyboard(self, event):
        self.logger.debug("Remove Keyboard: %s", event)
        self.place_forget()
    
        
        
        