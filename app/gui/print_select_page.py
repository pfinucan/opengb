import Tkinter as tk
import ttk, tkFont
import time
import datetime
from gui.resources import Frame, Label, FixedFrame, LogoButton
import logging

# create logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class PrintSelectPage(Frame):
    def __init__(self, parent, *args, **kwargs):
    
        self.logger = logger
        
        Frame.__init__(self, parent.fr_right, *args, name="print select", **kwargs)

        self.parent = parent
        self.state = None

        self.buttons = []
        self.selected_file = None
        self.selected_file = []
        label_widths = [20, 10, 10]
        
        self.bt_frame = Frame(self, style='white.TFrame', width=205, height=420)
        self.bt_frame.grid( row=0, column=0, sticky='NSEW')
        
        self.cloud = LogoButton(self.bt_frame, "CLOUD", self.parent.icons['CLOUD'], 
                                button_type="vertical", width=205)
        self.cloud.config(width=400, height=210 )
        self.cloud.grid(row=0, column=0,  sticky="NSEW") 

        #self.grid_propagate(0)
        #tk.Grid.columnconfigure(self, 0, weight=0)

        self.usb = LogoButton(self.bt_frame, "USB", self.parent.icons['USB'], button_type="vertical" )
        self.usb.grid(row=1, column=0,  sticky="NSEW")
        
        
        #file list
        self.files_frame = Frame(self, padding=(3), style='darkgray.TFrame' )
        self.files_frame.grid(row=0, column=1, rowspan=2, sticky="NSEW")
 
        self.files_list_frame = Frame(self.files_frame, padding=(5), style='darkgray.TFrame' )
        self.files_list_frame.grid(row=0, column=0, sticky="NSEW") 
        # override default width sharing
        tk.Grid.rowconfigure(self.files_frame, 1, weight=10) 
        
        self.files_action_frame = Frame(self.files_frame, padding=(5), style='gray.TFrame' )
        self.files_action_frame.grid(row=1, column=0, sticky="EWS") 
        
        self.load_bt = LogoButton(self.files_action_frame, "Load", self.parent.icons['nav_fwd'], state="active", size="small"  )
        self.load_bt.grid(row=0, column=0,  sticky="E", ipady=15)

        self.file_lines = []
        self.file_labels = []
        self.file_vars = []

        for i in range(6):
            self.file_lines.append(Frame(self.files_list_frame, style='dark_gray.TFrame', padding=(10,2)))
            temp = []
            for n in range(3):
                temp.append(Label(self.file_lines[-1], padding=(10), 
                                  style="files.TLabel", text=" ", 
                                  width=label_widths[n])
                            )
                temp[-1].grid(row=0, column=n, sticky="NWE")
                #temp[-1].grid_propagate(0)
                #tk.Grid.columnconfigure(self.file_lines[-1], n, weight=1)
                
            self.file_labels.append(temp)
            self.file_lines[-1].grid(row=i, stick='WE')
            #self.file_lines[-1].grid_propagate(0)
            #tk.Grid.rowconfigure(self.file_lines[-1], i, weight=1)
            
        self.cloud.bind("<Button-1>", self.switch_to_cloud)
        self.usb.bind("<Button-1>", self.switch_to_USB)
        #self.page()
        self.switch_to_cloud(1)

    def switch_to_cloud(self, e):
        self.logger.debug('Switch to cloud:')
        if not self.state == "cloud":
            self.state = "cloud"
            self.cloud.set_state("active")
            self.usb.set_state(None)
            self.update_file_list("cloud")
            
    def switch_to_USB(self, e):
        self.logger.debug('Switch to USB:')
        if not self.state == "USB":
            self.state = "USB"
            self.usb.set_state("active")
            self.cloud.set_state(None)
            self.update_file_list("USB")

    def page(self, e):
        
        if self.parent.dispatcher.bot.gcode_file_name:
            self.page_two(self.selected_file)
        
        else:
            self.configure(style='gray.TFrame')
            self.grid_propagate(0)
            self.grid(sticky='NSEW')
            self.configure(height=420, width=700)

            #self.cloud.set_state("active")

            self.update_file_list()
            
            # update nav
            self.parent.var_home_logo.set(unichr(61523))       
            self.parent.var_home.set("PRINT")
            self.parent.menu_page.home.configure(style="yellow.TLabel")

    def unpage(self):
        self.parent.empty_page(self.parent.fr_right)
        #for idx, button in enumerate(self.buttons):
        #    button.grid_remove()
            
    def update_file_list(self, source=None):
        if source == None:
            source = self.state
        file_table = self.parent.db.Files
        if source == "cloud":
            self.files = [{"uuid":file.uuid,
                           "name":file.name, 
                           "file_size":file.file_size, 
                           "file_mtime":file.file_mtime, 
                           "created_date":file.created_date.strftime("%m/%d/%Y %H:%M") 
                           } for file in file_table.select().where(file_table.file_location == "cloud").order_by(file_table.created_date.desc())]      
     
        if source=="USB":
            self.files = self.parent.dispatcher.usb.files()
            
        for label in self.file_labels:
            label[0].text = ""
            label[1].text = ""
            label[2].text = ""
            
        for idx, afile in enumerate(self.files):
            if idx > 5:
                break
            self.file_labels[idx][0].text = afile['name']
            self.file_labels[idx][1].text = datetime.datetime.strptime( time.ctime(float(afile['file_mtime'])), "%a %b %d %H:%M:%S %Y") 
            #self.file_labels[idx][1].text = datetime.datetime.fromtimestamp( time.ctime(float(afile['file_mtime']))) 
            self.file_labels[idx][2].text = self.sizeof_fmt(float(afile['file_size']))
            self.file_lines[idx].bind("<Button-1>", lambda event, clousure_var=self.files[idx]: self.load_print(clousure_var, event))
        
    def cap(self, s, l):
        return s if len(s)<=l else s[0:l-3]+'...' 
        
    def load_print(self, file_info, event):
        self.logger.info('Loading print: %s', event)
        
        if file_info['name']:
            if self.state == "USB":
                file_info = self.parent.dispatcher.add_print_to_db( file_info['file_path'] )
            file_details = self.parent.dispatcher.load_print(file_info['uuid'])
            
            file_info['details'] = file_details
            self.selected_file = file_info
            self.logger.info('File into: %s', file_info)
            self.page_two(self.selected_file)

    def page_two(self, file_info):
        self.unpage()
        self.parent.print_run_page.page(file_info) 
        
    def sizeof_fmt(self, num, suffix='B'):
        try:
            for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
                if abs(num) < 1024.0:
                    return "%3.1f%s%s" % (num, unit, suffix)
                num /= 1024.0
            return "%.1f%s%s" % (num, 'Yi', suffix)
        except:
            self.logger.debug('error in file size calc')