import time
import Tkinter as tk
import ttk, tkFont
from resources import Frame, Label, FixedFrame, LogoButton
import logging

logger = logging.getLogger(__name__)

class OverlayPage(ttk.Frame):
    def __init__(self, parent, *args, **kwargs):
        self.logger =  logger
        
        self.parent = parent
        ttk.Frame.__init__(self, parent, *args, width=700, height=480, style='white.TFrame', **kwargs)
        self.grid_propagate(0)
        
        self.deployed = False
        self.drag = {'ing':False, 'start':0, 'end':0, 'x':0, 'y':0, 'x1':0,'y1':0}
        self.deployed_x = 300
        self.speed = 50
        self.edge = self.parent.sizes['screen_width'] 
        self.target = self.edge
        self.tab_offset = 44

        # Swipe test 
        self.place(x=self.edge, y=0) 
        #self.parent.bind_all("<ButtonRelease-1>", self.onSwipeEnd)
        #self.parent.bind_all('<B1-Motion>', self.onSwipe)
        
        self.tab = Label(self.parent, text=self.parent.icons['nav_back'], style='white.logo.TLabel')
        self.tab.place(x=self.edge - self.tab_offset, y=0)
        self.tab.bind('<1>', self.toggle)
        
        
        
        # ################ Start of old code ################################################
        self.buttons = []
        self.XY_frame = tk.LabelFrame(self, text="X Y Control")
        self.XY_frame.grid(row=1, columnspan=7, sticky='W', padx=5, pady=5, ipadx=5, ipady=5)
        
        tk.Button(self.XY_frame, text='+Y', width=4, height=3, command=lambda: self.bump("Y")).grid(row=1, column=2, sticky='WE')
        tk.Button(self.XY_frame, text='-X', width=4, height=3, command=lambda: self.bump("X-")).grid(row=2, column=1, sticky='WE')
        tk.Button(self.XY_frame, text='+X', width=4, height=3, command=lambda: self.bump("X")).grid(row=2, column=3, sticky='WE')
        tk.Button(self.XY_frame, text='-Y', width=4, height=3, command=lambda: self.bump("Y-")).grid(row=3, column=2, sticky='WE')
        
        self.Z_frame = tk.LabelFrame(self, text="Z Control")
        self.Z_frame.grid(row=1, column=8, columnspan=1, sticky='W', padx=5, pady=5, ipadx=5, ipady=5)
        tk.Button(self.Z_frame, text='+Z', width=4, height=3, command=lambda: self.bump("Z")).grid(row=1, column=1, sticky='WE')
        tk.Button(self.Z_frame, text='-Z', width=4, height=3, command=lambda: self.bump("Z-")).grid(row=2, column=1, sticky='WE')

        self.step_size = tk.LabelFrame(self, text="Step Size")
        self.step_size.grid(row=1, column=9, columnspan=1, sticky='W', padx=5, pady=5, ipadx=5, ipady=5)
        
        self.bump_sizes = ["0.1", "1", "10"]
        for n, size in enumerate(self.bump_sizes):
            self.logger.debug('Movement bump size: %s, %s', size, n)
            button = tk.Button(self.step_size, text=size, width=4, height=3, 
                          command=lambda n=n, size=size: self.set_bump(n, size))
            button.grid(row=n+1, column=1, sticky='WE')
            self.buttons.append(button)
            
        self.heat_frame = tk.LabelFrame(self, text="Preheat Settings")
        self.heat_frame.grid(row=1, column=10, columnspan=1, sticky='W', padx=5, pady=5, ipadx=5, ipady=5)   
        tk.Button(self.heat_frame, text='Preheat 200C', width=14, height=2, command=lambda: self.send_gcode("M104 S200")).grid(row=1, column=1, sticky='WE')
        tk.Button(self.heat_frame, text='Heat Off', width=14, height=2, command=lambda: self.send_gcode("M104 S0")).grid(row=2, column=1, sticky='WE')

        tk.Button(self.heat_frame, text='Preheat Bed', width=14, height=2, command=lambda: self.send_gcode("M140 S55")).grid(row=3, column=1, sticky='WE')
        tk.Button(self.heat_frame, text='Bed Off', width=14, height=2, command=lambda: self.send_gcode("M140 S0")).grid(row=4, column=1, sticky='WE')

        tk.Button(self.heat_frame, text='Motors Off', width=14, height=2, command=lambda: self.send_gcode("M18")).grid(row=5, column=1, sticky='WE')
        tk.Button(self.heat_frame, text='Fan ON', width=14, height=2, command=lambda: self.send_gcode("M106")).grid(row=7, column=1, sticky='WE')
        tk.Button(self.heat_frame, text='Fan off', width=14, height=2, command=lambda: self.send_gcode("M107")).grid(row=7, column=1, sticky='WE')

        self.home_frame = tk.LabelFrame(self, text="Homing")
        self.home_frame.grid(row=2, column=1, columnspan=10, sticky='WE', padx=5, pady=5, ipadx=5, ipady=5)   
        tk.Button(self.home_frame, text='Home X', width=5, height=2, command=lambda: self.send_gcode("G28 X")).grid(row=1, column=4, sticky='WE')
        tk.Button(self.home_frame, text='Home Y', width=5, height=2, command=lambda: self.send_gcode("G28 Y")).grid(row=1, column=3, sticky='WE')
        tk.Button(self.home_frame, text='Home Z', width=5, height=2, command=lambda: self.send_gcode("G28 Z")).grid(row=1, column=2, sticky='WE')
        tk.Button(self.home_frame, text='Home All', width=5, height=2, command=lambda: self.send_gcode("G28")).grid(row=1, column=1, sticky='WE')
        
        tk.Button(self.home_frame, text='E+', width=5, height=2, command=lambda: self.bump("E")).grid(row=2, column=1, sticky='WE')
        tk.Button(self.home_frame, text='E-', width=5, height=2, command=lambda: self.bump("E-")).grid(row=2, column=2, sticky='WE')
        
        tk.Button(self.home_frame, text='RESET', width=5, height=2, command=lambda: self.reset()).grid(row=2, column=3, sticky='WE')

        tk.Button(self.home_frame, text='Delete Current', width=5, height=2, command=self.delete_file).grid(row=4, column=1, sticky='WE')
        
        #print len(self.size_bns) 
        self.set_bump(2,"10")
    
    def delete_file(self):
        file_id = self.parent.print_run_page.file_info['id']
        self.logger.debug('file to delete: %s', file_id)
        self.parent.dispatcher.delete_print_file(id=file_id)
        
    def reset(self):
        self.logger.debug('Restarting bot!!!')
        self.parent.dispatcher.restart_bot()
        
    def bump(self, part_gcode):
        gcode = "G1 " + part_gcode + self.bump_size
        print "bump", gcode
        self.parent.dispatcher.write_gcode_priorty("G91")
        self.parent.dispatcher.write_gcode_priorty(gcode)  
        self.parent.dispatcher.write_gcode_priorty("G90")
        
    def set_bump(self, n, size):
        self.logger.debug('Set bump, index size: %s, %s', n, size)

        self.bump_size = size
        for index, button in enumerate(self.buttons):
            if index == n:
                button.config(state="disabled")
            elif button['state'] == "disabled":
                button.config(state="active")
                
    def send_gcode(self, gcode):
        self.logger.debug("send gcode: %s", gcode)
        self.parent.dispatcher.write_gcode_priorty(gcode)
        
        
    # ################ End of old code ################################################
    def toggle(self, e):
        if self.deployed:
            self.deployed = False
            self.target = self.edge 
            self.tab.text = self.parent.icons['nav_back']
        else:
            self.deployed = True
            self.target = self.deployed_x 
            self.tab.text = self.parent.icons['nav_fwd']
        self.logger.debug("toggle: %s", self.deployed)
        self.update_location()
            
    def update_location(self):
        x_val = int(self.place_info()['x'])
        if self.deployed and x_val > self.target:
            x_edge = x_val - self.speed
            self.place(x=x_edge)
            self.tab.place(x=x_edge - self.tab_offset)
            self.parent.after(50, self.update_location)
        if not self.deployed and x_val < self.target:
            x_edge = x_val + self.speed
            self.place(x=x_edge)
            self.tab.place(x=x_edge - self.tab_offset)
            self.parent.after(50, self.update_location)
        
    def onSwipe(self, event):
        #print 'Widget=%s X=%s Y=%s' % (event.widget, event.x, event.y)
        x = int(event.x_root) - int(self.parent.winfo_rootx())
        y = int(event.y_root) - int(self.parent.winfo_rooty())

        if self.drag['ing'] == False:
            self.drag['ing'] = True
            self.drag['start'] = time.clock()
            self.drag['x'] = x
            self.drag['y'] = y
        dx = x - self.drag['x']
        dy = y - self.drag['y']
  
        
        #print 'X0=%s X=%s  Y0=%s Y=%s ' % (event.x_root, event.x, event.y_root, event.y)
        #print dx, dy
        if not self.deployed and self.drag['x']  > self.edge - 100:
            self.logger.debug("deploy")
            # Overlay is retracted and swiped started on the right side
            if abs(dx) > abs(dy): # moving mostly left - right 
                if dx < 0:
                    self.logger.debug("Swipe left")
                    if x > self.deployed_x:
                        self.place(x=x)
                    elif x > self.edge:
                        self.place(x=self.edge)
                        self.deployed = False
                    else:
                        self.place(x=self.deployed_x)
                        self.deployed = True
                    
        if self.deployed and self.drag['x']  > self.deployed_x:
            # Overlay is deployed and swiped is to the right
            if abs(dx) > abs(dy): # moving mostly left - right 
                if dx > 0:
                    self.logger.debug("Swipe right")
                    if self.drag['x'] < (self.edge - 100) and self.drag['x'] > self.deployed_x - 200:
                        self.place(x=x)
                            
        
    def onSwipeEnd(self, event):
        x = int(self.place_info()['x'])
        
        self.drag['end'] = time.clock()
        dt = self.drag['end'] - self.drag['start']
        
        self.drag['ing'] = False
        
        if x > self.deployed_x or not self.deployed:
            self.swipe_retract(event)
        
    def swipe_retract(self, event):
        x_val = int(self.place_info()['x'])
        self.deployed = False
        if x_val < self.edge:
            self.place(x=x_val + self.speed)
            self.parent.after(50, self.swipe_retract, event)