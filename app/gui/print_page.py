from __future__ import division
import config.settings as settings
import Tkinter as tk
import ttk, tkFont
from .resources import Frame, Label, FixedFrame, LogoButton
import logging

# create logger
logger = logging.getLogger(__name__)

class PrintPage(ttk.Frame):
    def __init__(self, parent, *args, **kwargs):
    
        self.logger = logger
        
        ttk.Frame.__init__(self, parent.fr_right, *args, name="print", **kwargs)
        self.parent = parent
        
        # Direct access to bot
        self.bot = self.parent.dispatcher.bot

        self.lines = []
        self.lines.append(Frame(self, width=700, height=105, style='active.TFrame' ))
        self.lines.append(Frame(self, width=700, height=105, padding=15, style='gray.TFrame' ))
        self.lines.append(Frame(self, width=700, height=105, style='gray.TFrame' ))
        self.lines.append(Frame(self, width=700, height=105, style='default.TFrame' ))
        
        self.lines[0].grid(row=0, column=0, sticky="W")
        self.lines[1].grid(row=1, column=0, sticky="W")
        self.lines[2].grid(row=2, column=0, sticky="W")
        self.lines[3].grid(row=3, column=0, sticky="W")

        # First line text
        self.temperature_frame = Frame(self.lines[0], width=250, height=105, style='gray.TFrame', padding=(25,5) )
        self.temperature_frame.grid(row=0, column=1)
        self.text_frame = Frame(self.lines[0], width=450, height=105, style='active.TFrame' )
        self.text_frame.grid(row=0, column=0)

        self.file_name = Label(self.text_frame, text='', style='active.TLabel')
        self.file_size = Label(self.text_frame, text='', style='active.TLabel')
        self.print_time = Label(self.text_frame, text='', style='active.TLabel')

        Label(self.temperature_frame, text='TEMP', style="small.blackOngray.TLabel").grid(row=0, column=1, sticky='WS')        
        Label(self.temperature_frame, text=self.parent.icons['flame'], style="blackOngray.logo.TLabel").grid(row=1, column=0) 
        Label(self.temperature_frame, text=self.parent.icons['degree'], style="blackOngray.logo.TLabel").grid(row=1, column=3)
        self.temperature = Label(self.temperature_frame, text="0", style="large.blackOngray.TLabel")
        self.temperature.grid(row=1, column=1)
        
        self.progress_bar = Frame(self.lines[2], style='DarkGray.TFrame', padding=(2,2) )  
        self.progress_bar.grid(sticky='NSEW', padx=25, pady=10)

        self.progress_insert = Frame(self.progress_bar, style='white.TFrame', width=1, height=90 )
        self.progress_insert.grid(sticky='W')
        
        self.progress_image = tk.PhotoImage(file=settings.STATIC_PATH + "/progress.gif")
        ttk.Label(self.progress_insert, image=self.progress_image, style="gray.TLabel").grid()
        
        Label(self.lines[1], text='PROGRESS:', style="small.blackOngray.TLabel").grid(row=0, column=0, sticky='WS')
        self.progress = Label(self.lines[1], text="0%", style="large.blackOngray.TLabel")
        self.progress.grid(row=1, column=0)
        
        Label(self.lines[1], text='TIME TO FINISH:', style="small.blackOngray.TLabel").grid(row=0, column=3, sticky='ES')
        self.time_left = Label(self.lines[1], text="0:0:00 MIN", style="large.blackOngray.TLabel")
        self.time_left.grid(row=1, column=3, sticky='E')
        tk.Grid.columnconfigure(self.lines[1], 1, weight=1)
        
        self.settings_button = FixedFrame(self.lines[3], text_var=["CANCEL"], size=[105, 232] )
        self.pause_button = FixedFrame(self.lines[3], text_var=["PAUSE", self.parent.icons['pause']], size=[105, 232] )
        self.print_button = FixedFrame(self.lines[3], text_var=["PRINT", self.parent.icons['play']], size=[105, 232] )
        
        self.settings_button.bind("<1>", self.cancel_print)
        self.print_button.bind("<1>", self.start_print)
        self.pause_button.bind("<1>", self.pause_print)

        #self.progress = FixedFrame(self.lines[1], frame_style='blackOngray.TFrame', text_var=["progress", "47%", "test"], size=[105, 300] )

    def page(self, file_info):
        self.file_info = file_info
        self.logger.debug('Print page loaded, file: %s', file_info)
        progress_int = 30
        temp = self.create_duration(self.file_info['details']['duration'], 'short', scale=(100 - progress_int))
        self.logger.debug('Test duration: %s', temp)

        self.file_name.text = file_info['name']
        self.file_size.text = '56K'
        self.print_time.text = self.create_duration(file_info['details']['duration'], 'long')
        
        self.time_left.text = self.create_duration(file_info['details']['duration'], 'short')
        
        self.grid()
        tk.Grid.rowconfigure(self, 1, weight=1)
        tk.Grid.rowconfigure(self, 2, weight=1)
        tk.Grid.rowconfigure(self, 3, weight=1)
        tk.Grid.rowconfigure(self, 4, weight=1)
        
        tk.Grid.columnconfigure(self.lines[0], 1, weight=1)
        tk.Grid.columnconfigure(self.lines[0], 2, weight=1)
        tk.Grid.columnconfigure(self.lines[0], 3, weight=1)
        
        self.file_name.grid(row=1, column=1)
        self.file_size.grid(row=1, column=2)
        self.print_time.grid(row=1, column=3)
        
        self.settings_button.grid(row=1, column=1)
        self.pause_button.grid(row=1, column=2)
        self.print_button.grid(row=1, column=3)
        
        tk.Grid.columnconfigure(self.lines[3], 1, weight=1)
        tk.Grid.columnconfigure(self.lines[3], 2, weight=1)
        tk.Grid.columnconfigure(self.lines[3], 3, weight=1)

        self.progress.grid()
        
        # update nav
        self.parent.var_home_logo.set(unichr(61523))
        self.parent.var_home.set("PRINT  / ")
        self.parent.var_layer_1.set(file_info['name'])
        self.parent.menu_page.home.configure(style="TLabel")
        self.parent.menu_page.home_layer_1.configure(style="yellow.TLabel")

        self.parent.after(1000, self.update_page)
    
    def start_print(self, e):
        self.print_button.set_style('active.TFrame')
        self.pause_button.set_style('TFrame')
        self.parent.dispatcher.start_print()
        
        
    def pause_print(self, e):
        self.pause_button.set_style('active.TFrame')
        self.print_button.set_style('TFrame')
        self.parent.dispatcher.stop_print()
        
    def cancel_print(self, e):
        if self.bot.printing:
            self.bot.stop_printing()
        self.file_info = None
        self.bot.gcode_file_name = None
        self.unpage()
        self.parent.print_page.page(None)
    
    def unpage(self):
        self.parent.empty_page(self.parent.fr_right)
        #for idx, button in enumerate(self.buttons):
        #    button.grid_remove() 
        
    def update_page(self):
        """Method to check for changes in the printer
        """
        self.logger.debug('update page function')
        if not self.file_info:
            self.logger.debug('update page function exit because no file info')
            return

        status = self.parent.dispatcher.status
        temperature = self.bot.get_temperature(repeat=True)
        try:
            progress_int = int((self.bot.line_number / self.bot.print_lines)*100)
        except:
            print self.bot.print_lines
            progress_int = 0
        if progress_int == 0 and self.bot.line_number > 0:
            progress_int = 1
        width_scale = int((progress_int * 650 ) / 100)
        self.progress_insert.config(width=width_scale)
        progress = str(progress_int) + "%"
        
        self.time_left.text = self.create_duration(self.file_info['details']['duration'], 'short', scale=( 100 - progress_int))

        if self.bot.heating:
            progress = "Heating..."
        self.progress.text = progress
        if "Extruder" in temperature:
            #print temperature["Extruder"]["temperature"]
            temp_string = temperature["Extruder"]["temperature"] + " (" + temperature["Extruder"]["setpoint"] + ")"
            self.temperature.text = temperature["Extruder"]["temperature"]
            self.logger.debug('temperature string: %s', temp_string)

        if self.bot.printing:
            self.parent.dispatcher.update_print(progress = progress_int, temperature = self.temperature.text)
            
        if self.grid_info():
            # only continue if page is current
            self.parent.after(1000, self.update_page)
            
        self.logger.debug('End of update page function: progress_int: %s, line number: %s', progress_int, self.bot.line_number)

            
    def create_duration(self, duration, time_format, scale=None):
        d = duration.split(':')
        seconds = (int(d[0])*24*60*60) + (int(d[1])*60*60) + (int(d[2])*60) + (int(d[3]))
        if scale:
            seconds = int(scale*seconds/100)
            duration = self.parent.convert_time(seconds)
            d = duration.split(':')
        if time_format == 'long':
            time_string = ""
            if int(d[0]) > 0:
                time_string = time_string + d[0] + " days, "
            if int(d[1]) > 0:
                time_string = time_string + d[1] + " hrs, "
            time_string = time_string + d[2] + " min"
        if time_format == 'short':
            d = duration.split(':')
            time_string = ""
            if int(d[0]) > 0:
                time_string = time_string + d[0] + ":"
            if int(d[1]) > 0:
                time_string = time_string + d[1] + ":"
            time_string = time_string + d[2] + " MIN"
        return time_string