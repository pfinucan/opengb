import config.settings as settings
import Tkinter as tk
import ttk, tkFont
from gui.resources import Frame, Label, FixedFrame, LogoButton
import logging

logger = logging.getLogger(__name__)

class HomePage(Frame):
    def __init__(self, parent, *args, **kwargs):
        self.logger = logger
        
        kwargs['style'] = 'gray.TFrame'
        
        Frame.__init__(self, parent.fr_right, *args, name="home", **kwargs)
        
        self.parent = parent
        self.gigabot = tk.PhotoImage(file=settings.STATIC_PATH + "gb_front_page.gif")
        
        ttk.Label(self, image=self.gigabot, style="gray.TLabel").grid(sticky='NSEW')

        #self.page()

    def page(self, e):
        self.logger.info("home paged")

        self.parent.tools_page.grid_remove()
        #Set Top menu butons
        self.parent.var_home_logo.set(unichr(61559)) # v up
        self.parent.var_home.set("GIGABOT")
        self.parent.menu_page.home.configure(style="TLabel")
        self.parent.var_layer_1.set("")
        
        self.parent.empty_page(self.parent.fr_right)
        #self.parent.fr_right.configure(style='gray.TFrame')
        self.grid(sticky='NSEW')

    def unpage(self):
        self.grid_remove()