from __future__ import division
import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
import Tkinter as tk
import ttk, tkFont
from PIL import Image
from subprocess import Popen, call
import controllers.dispatch as dispatch
import time
import utils.ip
import database.db
import config.style as TStyle
from .resources import Frame, Label, FixedFrame, LogoButton
from .menu import Menu
from .status_page import StatusPage
from .tools_page import ToolsPage
from .settings_page import SettingsPage
from .print_select_page import PrintSelectPage
from .print_page import PrintPage
from .home_page import HomePage
from .overlay_page import OverlayPage
from .extra_overlay_page import ExtraOverlayPage
from .keyboard import Keyboard
import logging

# create logger
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

#calibration work around
if not os.name == 'nt':
    logger.info("Calibration...") 
    code = call("/opt/scripts/3rdparty/xinput_calibrator_pointercal.sh")
    logger.info("Calibration output: %s", code)    
    

class MainApplication(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args,  **kwargs)
        
        self.parent = parent
        self.parent.title('test')
        self.parent.height = 480
        self.width =  self.winfo_screenwidth()
        self.height =  self.winfo_screenheight()
        if self.width > 800: 
            logger.info("Screen size: %s", self.width )
        else:
            self.parent.attributes("-fullscreen", True)
        self.parent.configure(background = 'red')

        #other assets
        self.db = database.db
        self.ip = utils.ip
        self.dispatcher = dispatch.Dispatch()
        self.dispatcher.start_server_listening_thread()
        
        self.fonts = {}
        self.colors = {}
        
        self.create_styles(TStyle.style)
        self.sizes = TStyle.sizes
        self.icons = TStyle.icons

        # Top Level Frames
        self.master_frame = tk.Frame(self, 
                                     width=self.sizes['screen_width'], 
                                     height=self.sizes['screen_height'])
        self.master_frame.grid(row=0, column=0)

        self.fr_info = Frame(self.master_frame, width=800, height=60)
        self.fr_info.grid(row=0, column=0, columnspan=2, sticky='WE')
        #self.fr_info.configure(style='active.TFrame')

        self.fr_left = Frame(self.master_frame, width=self.sizes['column_left'], height=420, style='active.TFrame')
        self.fr_left.grid(row=1, column=0, sticky='WE')
        
        self.fr_right = Frame(self.master_frame, width=self.sizes['column_right'], height=420, style='white.TFrame' )
        self.fr_right.grid(row=1, column=1) 
        tk.Grid.rowconfigure(self.fr_right,0, weight=1)
        tk.Grid.columnconfigure(self.fr_right,0, weight=1)

        # Top menu_page string vars These are placed in the menu_page class    
        self.var_home_logo = tk.StringVar()        
        self.var_home = tk.StringVar()
        self.var_layer_1 = tk.StringVar()
        self.var_layer_2 = tk.StringVar()
        self.var_layer_3 = tk.StringVar()
        
        # Pages
        self.home_page = HomePage(self)        
        self.print_page = PrintSelectPage(self)
        self.print_run_page = PrintPage(self)
        self.status_page = StatusPage(self)
        self.tools_page = ToolsPage(self)
        self.settings_page = SettingsPage(self)
        self.menu_page = Menu(self)
        self.overlay_page = OverlayPage(self)
        #self.keyboard = Keyboard(self)


        #configure frist page
        self.menu_page.button_home(1) 

    def quit(self):
        sys.exit()
        
    def empty_page(self, frame):
        childern = frame.winfo_children()
        for child in childern:
            child.grid_remove()  
            
    def create_styles(self, new_style):
        self.tk_style = ttk.Style()
        self.tk_style.theme_create( new_style['name'], parent="alt")
        self.tk_style.theme_use(new_style['name'])
        
        self.colors = new_style['colors']
        
        for font in new_style['fonts']:
            self.fonts[font] = tkFont.Font( **new_style['fonts'][font] )
        
        for style in new_style['styles']:
            args = new_style['styles'][style]
            if 'font' in args:
                font = args.pop('font')
                if font in self.fonts:
                    args['font'] = self.fonts[font]
            # overwrite colors if they are in the color list
            if 'background' in args:
                if args['background'] in self.colors:
                    args['background'] = self.colors[args['background']]
            if 'foreground' in args:
                if args['foreground'] in self.colors:
                    args['foreground'] = self.colors[args['foreground']]
            
            self.tk_style.configure(style, **args)
        
        #print style
        
    def convert_time(self, seconds):
        m, s = divmod(seconds, 60)
        h, m = divmod(m, 60)
        d, h = divmod(h, 24)
        return "%d:%d:%02d:%02d" % (d, h, m, s)


def main():
    global main_frame
    root = tk.Tk()
    root.geometry("800x480")
    main_frame = MainApplication(root)
    main_frame.pack(side="top", fill="both", expand=True)
    root.mainloop()
    exit()
        
if __name__ == "__main__":
    main()
