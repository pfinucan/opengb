import Tkinter as tk
import ttk, tkFont
from gui.resources import Frame, Label, FixedFrame, LogoButton
import logging

logger = logging.getLogger(__name__)


class ToolsPage(Frame):
    def __init__(self, parent, *args, **kwargs):
        self.logger = logger
        
        kwargs['style'] = 'active.TFrame'
        Frame.__init__(self, parent.fr_right, *args, name="tools", **kwargs)
        self.parent = parent
        self.buttons = {}
        
        self.commands = { "Server" : self.toggle_server,
                          "Socket":  self.toggle_socket,
                          "Proxy Lighting": self.toggle_proxy,
                          "Temperature Lighting": self.toggle_temp_lighting,
                          }
        
        for idx, button_text in enumerate(self.commands):
            print button_text
            self.buttons[button_text] = (LogoButton(self, button_text, self.parent.icons['off'], style='active'))
            self.buttons[button_text].grid(row=idx, column=0)
            self.buttons[button_text].bind("<Button-1>", self.commands[button_text])
            
        self.logger.debug("Tools page init: %s", self)

    def page(self, e):
        if self.parent.dispatcher.checkserver():
            self.buttons['Server'].logo.text = self.parent.icons['on']
        else:
            self.buttons['Server'].logo.text = self.parent.icons['off']

            
        self.grid_propagate(0)
        self.grid(sticky='NSEW')
        self.configure(height=420, width=700)

        # update nav
        self.parent.var_home_logo.set(unichr(61523))       
        self.parent.var_home.set("TOOLS")
        self.parent.menu_page.home.configure(style="yellow.TLabel")
        
        self.logger.debug("Tools page load: %s", self)

    def unpage(self):
        self.parent.empty_page(self.parent.fr_right)
        self.grid_remove()

    def toggle_server(self, event):
        self.logger.debug("server button")
        if self.parent.dispatcher.server_running:
            self.logger.debug("Gui thinks the server is running")
            self.buttons['Server'].logo.text = self.parent.icons['off']
            self.parent.dispatcher.killserver()
        else:
            self.logger.debug("Gui thinks the server is NOT running")
            self.buttons['Server'].logo.text = self.parent.icons['on']
            self.parent.dispatcher.startserver()

    def toggle_socket(self, event):
        self.logger.debug("socket button")
        if  hasattr(self.parent, 'dispatcher'):
            if self.parent.dispatcher.socket_passthrough_running:
                self.logger.debug("GUI stopping socket pass through")
                self.buttons['Socket'].logo.text = self.parent.icons['off']
                self.parent.dispatcher.end_socket_passthrough_thread()
            else:
                self.logger.debug("GUI starting socket pass through")
                self.buttons['Socket'].logo.text = self.parent.icons['on']
                self.parent.dispatcher.start_socket_passthrough_thread()
                
    def toggle_proxy(self, event):
        self.logger.debug("proxy button")
        if  hasattr(self.parent, 'dispatcher'):
            if self.parent.dispatcher.watch_dog.light_mode == "prox":
                self.logger.debug("Turning off proxity mode")
                self.buttons['Proxy Lighting'].logo.text = self.parent.icons['off']
                self.parent.dispatcher.watch_dog.light("non-prox")
            else:
                self.logger.debug("Turning on proxity mode")
                self.buttons['Proxy Lighting'].logo.text = self.parent.icons['on']
                self.parent.dispatcher.watch_dog.light("prox") 
                
    def toggle_temp_lighting(self, event):
        self.logger.debug("temp lighting button")
        if  hasattr(self.parent, 'dispatcher'):
            if self.parent.dispatcher.watch_dog.color_mode == "temp":
                self.logger.debug("Turning off temp mode")
                self.buttons['Temperature Lighting'].logo.text = self.parent.icons['off']
                self.parent.dispatcher.watch_dog.light("non-temp")
            else:
                self.logger.debug("Turning on temperature mode")
                self.buttons['Temperature Lighting'].logo.text = self.parent.icons['on']
                self.parent.dispatcher.watch_dog.light("temp")   
                self.parent.after(3000, self.checkback)
                
            
    def button_push(self, button_text, event):
        # Expand right frame
        print button_text
        
        self.commands[button_text].page(event) 
        print self.parent.fr_left.grid_size()
        
        
    def checkback(self):
        """Method to check for changes in the printer
        """
        self.logger.debug("temp lighting checkback")
        temperature = self.parent.dispatcher.bot.get_temperature()
        self.logger.debug("Temperature %s: ", temperature)
        if temperature:
            if "Extruder" in temperature:
                #print temperature["Extruder"]["temperature"]
                temp_value = int(float(temperature["Extruder"]["temperature"]))
                self.parent.dispatcher.watch_dog.set_temperature(temp_value)
            if self.parent.dispatcher.watch_dog.color_mode == "temp":
                self.parent.after(3000, self.checkback)