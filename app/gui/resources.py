import Tkinter as tk
import ttk, tkFont
import logging

class Frame(ttk.Frame):
    def __init__(self, parent, pos=None, *args, **kwargs):
        self.logger = logging.getLogger(__name__)
        
        ttk.Frame.__init__(self, parent, *args,  **kwargs)
        self.parent = parent
        self.vertical = True
        self.width = None
        self.height = None
        if 'width' in kwargs:
            self.width = kwargs['width']
        if 'height' in kwargs:
            self.height = kwargs['height']

        if self.width and self.height:
            self.grid_propagate(0)
        if pos:
            self.grid(row=pos[0],column=pos[1])
       
    def bind(self, *args, **kwargs):
        ttk.Frame.bind(self, *args, **kwargs)
        kids = self.winfo_children()
        for kid in kids:
            kid.bindtags((self,) + kid.bindtags())
        
    def grid(self, *args, **kwargs):
        ttk.Frame.grid(self, *args, **kwargs)
        row = int(self.grid_info()['row'])
        col = int(self.grid_info()['column'])
        tk.Grid.rowconfigure(self.parent, row, weight=1)
        tk.Grid.columnconfigure(self.parent, col, weight=1)
        
        if self.vertical:
            tk.Grid.columnconfigure(self.parent, col, weight=1)

class Label(object, ttk.Label):
    def __init__(self, parent, grid=False, **kwargs):
        self.logger = logging.getLogger(__name__)
        
        self._textvariable = tk.StringVar()
        self.vertical = None
        self.states = {}
        
        if 'text' in kwargs:
            text = kwargs.pop('text')
            self._textvariable.set(text)
            self._text = text
            kwargs['textvariable'] = self._textvariable
        if 'style' in kwargs:
            self._style = kwargs['style']
            self.states['normal'] = self._style
        ttk.Label.__init__(self, parent, **kwargs)
        self.parent = parent
        
        self._margin = 0
        self.current_state = None
        self._style = None
        
    @property
    def text(self):
        """I'm the 'text' property."""
        return self._text
        
    @text.setter
    def text(self, value):
        """Set the text by updating the internal StrValue"""
        self._text = value
        self._textvariable.set(self._text)  

    @property
    def margin(self):
        """ defines a labels margin (like css)"""
        return self._margin
        
    @margin.setter
    def margin(self, value):
        self._margin = value
        if len(value) == 1:
            padx = value
            pady = value
        if len(value) == 2:
            padx = value[0]
            pady = value[1]
        ttk.Label.grid(self, padx=padx, pady=pady)
        
    def set_state(self, state):
        if state in self.states:
            self.config(style=self.states[state])
            self._style = self.states[state]
            self.current_state = state
        else:
            self.logger.debug("Label: state isn't know")
        
        
    def grid(self, *args, **kwargs):
        ttk.Label.grid(self, *args, **kwargs)
        row = int(self.grid_info()['row'])
        col = int(self.grid_info()['column'])
        tk.Grid.rowconfigure(self.parent, row, weight=1)
        if self.vertical:
            tk.Grid.columnconfigure(self.parent, col, weight=1)
        
            
class FixedFrame(Frame):
    def __init__(self, parent, frame_type="horizontal", text_var=[], size=None, frame_style=None):
        self.logger = logging.getLogger(__name__)
        
        self.frame_type = frame_type
        self.parent = parent
        
        if not frame_style:
            frame_style = self.parent['style']
        style_arr = frame_style.split('.')
        for i,s in enumerate(style_arr):
            if s == 'TFrame':
                style_arr[i] = 'TLabel'
        label_style = '.'.join(style_arr)

        Frame.__init__(self, parent, class_='logo_button', style=frame_style)
           
        style = ttk.Style()
        background_color = style.lookup('blackOn.TFrame', 'background')
        
        self.grid_propagate(0) 
        
        if size:
            self['width'] = size[1]
            self['height'] = size[0]
            pass

        self.labels = []
        self.label_vars = []
        
        for idx in xrange(0, 6):
            self.label_vars.append( tk.StringVar() )
            self.labels.append(ttk.Label(self, style=label_style, textvariable=self.label_vars[-1]))
            #self.labels.configure(background=background_color)
            tk.Grid.columnconfigure(self, idx, weight=1)
            tk.Grid.rowconfigure(self, idx, weight=1)
  
        #if frame_type == "horizontal":
        self.labels[0].grid(row=1, column=1)
        self.labels[1].grid(row=1, column=2)
        self.labels[2].grid(row=1, column=3)

        for idx, text_value in enumerate(text_var):
            self.label_vars[idx].set(text_value)
            
    def set_style(self, style):
        self.config(style=style)
        kid_style = style.replace('TFrame', 'TLabel')
        self.logger.debug("Set style: %s", style)
        kids = self.winfo_children()
        for kid in kids:
            kid.config(style=kid_style)
            

class LogoButton(Frame):
    def __init__(self, parent, text, logo, button_type="horizontal", state=None, size=None, width=None, style=None):
        self.logger = logging.getLogger(__name__)
    
        self.button_type = button_type
        self.parent = parent
        
        self.frame_style = "TFrame"
        self.text_style = "TLabel"
        self.logo_style = "logo.TLabel"
        if style:
            self.frame_style = style + ".TFrame"
            self.text_style = style + ".TLabel"
            self.logo_style = style + ".logo.TLabel"
        
        self.in_between_padding = 35

        if button_type == "horizontal":
            bt_width = 300
            bt_height = 105
        elif button_type == "vertical":
            bt_width = 200
            bt_height = 105
        args = {'class_':'logo_button'}
        if width:
            args['width'] = width
        args['style'] = self.frame_style
            
        Frame.__init__(self, parent, **args) #, width=bt_width, height=bt_height
        
        if size:
            self.logger.debug("LogoButton Size: %s, self.text_style: %s, Logo style: %s", size, self.text_style, self.logo_style)
            self.text_style = size + '.' + self.text_style
            if size == 'small':
                self.in_between_padding = 10
        
        self.logo = Label(self, text=logo, style=self.logo_style)
        self.text = Label(self, text=text, style=self.text_style)
        
        if button_type == "horizontal":
            self.logo.grid(row=0, column=0, padx=(10, 0))
            self.text.grid(row=0, column=1, padx=(self.in_between_padding, 5))
            
        if button_type == "vertical":
            self.logo.grid(row=0, column=0, sticky='S')
            self.text.grid(row=1, column=0, padx=10, sticky='N')
            tk.Grid.columnconfigure(self, 0, weight=1)
            
        if state:
            self.logger.debug("State change: %s ", state)
            self.set_state(state)
            
        
    def set_state(self, state):
        if state == None:
            set_state = ""
        else:
            set_state = state + '.'
        if self.button_type == "horizontal":
            self.configure(style=set_state +  self.frame_style)
            self.logo.configure(style=set_state +  self.logo_style)
            self.text.configure(style=set_state +  self.text_style)
        elif self.button_type == "vertical":
            self.configure(style=set_state +  self.frame_style)
            self.logo.configure(style=set_state +  self.logo_style)
            self.text.configure(style=set_state +  self.text_style)