import Tkinter as tk
import ttk, tkFont
from resources import Frame, Label, FixedFrame, LogoButton

class StatusPage(Frame):
    def __init__(self, parent, *args, **kwargs):
        kwargs['style'] = 'active.TFrame'
        Frame.__init__(self, parent.fr_right, *args, name="status", **kwargs)
        self.parent = parent
        
        self.text_labels = ["Status:", "Temperature:", "IP Address:", "Current Print:"]
        self.labels =[]
        self.details = []
        self.detail_vars = []
        
        self.group_frame = Frame(self, style="active.TLabel")
        
        for label in self.text_labels:
            self.labels.append(Label(self.group_frame, padding=(10,0,0,0), style="active.TLabel", text=label))
            self.detail_vars.append(tk.StringVar())
            self.details.append(Label(self.group_frame, padding=(10,0,0,0), style="active.TLabel", textvariable=self.detail_vars[-1]))
        
        for idx, button in enumerate(self.labels):
            button.grid(row=idx, column=0, sticky="E", padx=5, pady=5)   
            self.details[idx].grid(row=idx, column=1, sticky="W", padx=5, pady=5)

        self.group_frame.grid()    
            
        self.detail_vars[2].set(self.parent.ip.get_lan_ip())

    def page(self, e):
        self.grid(sticky='NSEW')

        # update nav
        self.parent.var_home_logo.set(unichr(61523))       
        self.parent.var_home.set("STATUS")
        self.parent.menu_page.home.configure(style="yellow.TLabel")
        
        self.parent.after(1000, self.checkback)
        
        print self.grid_info()
        print self.configure()

    def unpage(self):
        self.parent.empty_page(self.parent.fr_right)

    def checkback(self):
        """Method to check for changes in the printer
        """
        status = self.parent.dispatcher.status
        self.parent.dispatcher.print_messages()
        
        temperature = self.parent.dispatcher.bot.get_temperature(repeat=True)
        if "Extruder" in temperature:
            #print temperature["Extruder"]["temperature"]
            temp_string = temperature["Extruder"]["temperature"] + " (" + temperature["Extruder"]["setpoint"] + ")"
            self.detail_vars[1].set(temp_string)
        if status:
            self.detail_vars[0].set(status)
        if self.grid_info():
            self.parent.after(1000, self.checkback)