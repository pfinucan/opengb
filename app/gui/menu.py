import Tkinter as tk
import ttk, tkFont
from gui.resources import Frame, Label, FixedFrame, LogoButton
import logging

# create logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

class Menu(ttk.Frame):
    def __init__(self, parent, *args, **kwargs):
        ttk.Frame.__init__(self, parent, *args,  **kwargs) 
        self.parent = parent
        
        self.active_page = parent.home_page
        
        self.bt_text = ["PRINT", "STATUS", "TOOLS", "SETTINGS"]
        self.buttons = []
        
        self.commands = { "PRINT" : self.parent.print_page,
                          "STATUS": self.parent.status_page,
                          "TOOLS": self.parent.tools_page,
                          "SETTINGS": self.parent.settings_page
                          }
        
        for idx, button_text in enumerate(self.bt_text):
            self.buttons.append(LogoButton(self.parent.fr_left, button_text, self.parent.icons[button_text]))
            self.buttons[idx].grid(row=idx, column=1, sticky='NSWE')
            self.buttons[idx].bind("<Button-1>", lambda event, clousure_var=button_text: self.button_push(clousure_var, event))
            
        #Special Home Button 61523 left 61559 up
        #String Var
        self.home_logo = Label(self.parent.fr_info, textvariable=self.parent.var_home_logo)
        self.home = Label(self.parent.fr_info, textvariable=self.parent.var_home)
        
        self.home_layer_1 = Label(self.parent.fr_info, textvariable=self.parent.var_layer_1)
        self.home_layer_2 = Label(self.parent.fr_info, textvariable=self.parent.var_layer_2)
        self.home_layer_3 = Label(self.parent.fr_info, textvariable=self.parent.var_layer_3)
        self.home = Label(self.parent.fr_info, textvariable=self.parent.var_home)
        self.home_logo.grid(row=0, column=0, padx=(30,0), sticky='W')
        self.home.grid(row=0, column=1, padx=(40,0), sticky='W')
        self.home_layer_1.grid(row=0, column=2, padx=(10,0), sticky='W')
        
        print self.parent.fr_info.grid_info()
        
        self.home_logo.bind("<Button-1>", self.button_home)

            
    def button_push(self, button_text, event):
        logger.info('Menu button pushed: %s', button_text)

        self.parent.fr_left.configure(width=self.parent.sizes['column_width_small'])
        self.parent.fr_right.configure(width=self.parent.sizes['column_width_large'])
        
        for button in self.buttons:
            button.set_state("default")
        if event.widget.winfo_class() == 'TLabel':
            widget = event.widget.master
        else: 
            widget = event.widget
        widget.set_state("active")
        
        self.active_page.unpage()
        self.active_page = self.commands[button_text]
        
        self.commands[button_text].page(event) 
        logger.debug('Left frame grid size: %s', self.parent.fr_left.grid_size())
        
    def button_home(self, event):
        logger.info('Menu home button pushed: %s', event)
        
        self.parent.fr_left.configure(width=self.parent.sizes['column_left'])
        self.parent.fr_right.configure(width=self.parent.sizes['column_right'])
        for button in self.buttons:
            button.set_state("default")
        self.parent.home_page.page(event)
        logger.debug('Frames in frame right: %s', self.parent.fr_right.grid_slaves())
        