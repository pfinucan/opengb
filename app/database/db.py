from peewee import *
import datetime
import os
import logging

logger = logging.getLogger(__name__)

PATH = os.path.dirname(os.path.abspath(__file__))
db = SqliteDatabase(PATH + '/db', threadlocals=True)

class BaseModel(Model):
    class Meta:
        database = db
        
    def dict(self):
        r = {}
        for k in self._data.keys():
          try:
             r[k] = getattr(self, k)
          except:
            logger.debug("oops, getattr not working: for %s in %s", k,  self._data.keys())

        return r

        
class Files(BaseModel):
    uuid = CharField(unique=True)
    name = CharField()
    file_location = CharField()
    file_path = CharField()
    file_mtime = CharField()
    file_size = DecimalField()
    created_date = DateTimeField(default=datetime.datetime.now)

    
class PrintFileDetails(BaseModel):
    file_id = ForeignKeyField(Files, related_name='details', to_field='uuid' )
    duration = CharField()
    distance = DecimalField()
    filament = DecimalField()
    print_time = CharField()
    layers = IntegerField()
    xmin = DecimalField()
    xmax = DecimalField()
    ymin = DecimalField()
    ymax = DecimalField()
    zmin = DecimalField()
    zmax = DecimalField()
    created_date = DateTimeField(default=datetime.datetime.now)
                
                
class Prints(BaseModel):
    print_file = ForeignKeyField(Files, related_name='prints', to_field='uuid')
    progress = DecimalField(default=0.0)
    modified = DateTimeField()
    created_date = DateTimeField(default=datetime.datetime.now)

    def save(self, *args, **kwargs):
        self.modified = datetime.datetime.now()
        return super(Prints, self).save(*args, **kwargs)
    
class DataPoints(BaseModel):
    print_file = ForeignKeyField(Prints, related_name='data_points')
    datatype = CharField()
    value = DecimalField(null = True)
    value_count = IntegerField(null = True)
    note = CharField(null = True)
    modified = DateTimeField()
    created_date = DateTimeField(default=datetime.datetime.now)

    def save(self, *args, **kwargs):
        self.modified = datetime.datetime.now()
        return super(DataPoints, self).save(*args, **kwargs)
    
class Settings(BaseModel):
    name = CharField()
    gcode = CharField()
    vector = CharField() 
    value = CharField()
    created_date = DateTimeField(default=datetime.datetime.now)
    
    class Meta:
        primary_key = CompositeKey('gcode', 'vector')
    
    def insert_dictionary(self, data):
        for gcode in data:
            for vector in data[gcode]:
                value = data[gcode][vector]
                rows = Settings.select().where(Settings.gcode == gcode, Settings.vector == vector)
                if rows.count() == 0:
                    newrow = Settings.create(name="None", gcode=gcode, vector=vector, value=value)
                else:
                    for row in rows:
                        row.value = value
                        row.save()
        else:
            return False
             
class Status(BaseModel):
    status = CharField()
    value = CharField( null=True )
    created_date = DateTimeField(default=datetime.datetime.now)
    
    def latest(self):
        return (Status
                .select()
                .order_by(Status.created_date.desc())
                .get())
    

def create_tables():
    tables = [Files, PrintFileDetails, Prints, DataPoints]
    for table in tables:
        try:
            table.create_table()
        except Exception, e:
            print e

def drop_tables():
    tables = [Files, PrintFileDetails, Prints, DataPoints]
    for table in tables:
        try:
            table.drop_table()
        except Exception, e:
            print e
        