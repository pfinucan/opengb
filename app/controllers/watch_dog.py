import sys
import os
import serial
import time
import threading
import Adafruit_BBIO.UART as UART
import logging

# create logger
logger = logging.getLogger(__name__)

WATCHDOG_BAUD = 9600
WATCHDOG_port="/dev/ttyO1" 

class WatchDog:
    def __init__(self, serial_buad=None, serial_port=None, loud=False, debug=False):
        UART.setup("UART1")
        if serial_port:
            self.serial_port = serial_port
        else:
            self.serial_port = WATCHDOG_port
        if serial_buad:
            self.serial_buad = serial_buad
        else:
            self.serial_buad = WATCHDOG_BAUD
        self.serial_timeout = 1
        self.messages = []
        self.priorty_command = []
        self.connected = False
        self.printing = False
        self.debug = debug
        self.loud = loud
        self.lock = threading.Lock()
        self.busy = False
        self.red = 100
        self.green = 100 
        self.blue = 100
        self.brightness = 0
        self.light_modes = {"prox": 1, "non-prox": 2, "temp": 3, "non-temp": 4}
        self.light_mode = "prox"
        self.color_mode = "non-temp"
        self.light_rgb_temperature = False
        
        self.encoder_position = 0


    def connect(self):
        """Create a connection to the serial port and launch the read thread
        """
        if self.debug:
            self.serial = serial.serial_for_url('loop://', timeout=self.serial_timeout)
        else:
            self.serial = serial.Serial()
            if not self.serial_port:
                ports = list(self.serial_ports())
                print ports
                if len(ports) > 1:
                    print ports
                    if '/dev/ttyO1' in ports:
                        self.serial.port = '/dev/ttyO1'
                else:
                    print ports
                    self.serial.port = ports[0]
            else:        
                self.serial.port = self.serial_port
            self.serial.baudrate = self.serial_buad
            self.serial.timeout = self.serial_timeout
            try:
                self.serial.open()
            except serial.SerialException, e:
                sys.stderr.write("Could not open serial port %s: %s\n" % (self.serial.portstr, e))
                print "running port search"
                print(list(self.serial_ports()))
                sys.exit(1)
            except OSError, e:
                print "serial error, checking serial ports"
                print(list(self.serial_ports()))
        self.alive = True
        self.thread_read = threading.Thread(target=self.reader)
        self.thread_read.setDaemon(True)
        self.thread_read.setName('read_serial')
        self.thread_read.start()
            
    def disconnect(self):
        """Stop copying"""
        if self.alive:
            self.alive = False
            self.connected = False
            del self.messages[:]
            self.thread_read.join()

    def reader(self):
        """Serial reading thread, loop forever and read serial"""
        while self.alive:
            #try:
            data = self.serial.readline()              # read one, blocking
            if data:
                if self.loud:
                    self.logger.info("in: %s ", data.strip())
                with self.lock:
                    if (data[:2] == "ok"):
                        if self.busy:
                            self.busy = False 
                            self.send_priorty_message()
                        if (data[:4] == "ok T"):
                            self.parse_temperature(data)
                    elif (data[:5] == "echo:"):
                        if (data[5:6] == "M"):
                            self.parse_settings(data[5:])
                        self.messages.append(data)     # Add message to list 
                    elif ( self.connected == False and data[:5] == "start"):
                        self.connected = True
                        del self.messages[:]               #empty messages
                        self.busy = False
                        if self.loud:
                            self.logger.info("CONNENTED")
                    else:
                        if self.loud or self.connected == False:
                            print(data)
                        if self.connected == True:
                            self.messages.append(data)     # Add message to list   
            #except:
            #    print("serial read error")
            #    #self.alive = False
        self.alive = False
    
    def send_message(self,command):
        """Send a single gcode command to serial port or return false if busy
        """
        if self.alive:
            with self.lock:
                if self.debug:
                    self.busy = False
                    return True
                if self.busy:
                    return False
                #try:
                if self.loud:
                    print "message " + command.strip()
                self.serial.write(command.strip() + "\n") 
                #self.busy = True
                #except serial.SerialException, e:
                #    print e
                return True

    def clear_busy(self):
        with self.lock:
            self.busy = False
            
    def light(self, mode, brightness=100, R=None, G=None, B=None):
        if R:
            self.red = R
        if G:
            self.green = G
        if B:
            self.blue = B 
        if mode in ["prox", "non-prox"]:
            self.light_mode = mode
        else:
            self.color_mode = mode
            
        self.brightness = brightness
        self.light_rgb_temperature = False
        command = "S" + str(self.light_modes[mode])
        print command
        self.send_message(command)
        lighting = "L" + str(self.brightness) + " R" + str(self.red) + " G" + str(self.green) + " B" + str(self.blue) 
        print lighting
        self.send_message(lighting)
        
    def set_temperature(self, temperature):
        if temperature > 100:
            temperature = 100
        lighting = "L" + str(self.brightness) + " T" + str(temperature)
        self.send_message(lighting)
        
            
if __name__ == "__main__":
    print sys.argv
    watch_dog = WatchDog(9600, serial_port="/dev/ttyO1" )
    watch_dog.connect()
    
    print "starting watchdog"
    for x in range(0, 10000):
        command = raw_input('Enter your input:')
        watch_dog.send_message(command)
        if command == "light":
            pass