import config.settings as settings
from subprocess import Popen
import socket
import threading
from printers.marlin import Marlin
import time
import json
import sys
import fileinput
from utils.file_management import USBFiles 
import os
import uuid
import database.db
import printers.gcode
import logging
import watch_dog

# create logger
logger = logging.getLogger(__name__)


HOST = 'localhost'    # The remote host
PORT = 8000              # The same port as used by the server
BAUDRATE = 115200 #250000 #115200 #250000 #  
if os.name == 'nt':
    #DEVICE = "COM6" #"COM5"  # "COM6" #
    BAUDRATE = 115200
else:
    #DEVICE = "/dev/ttyACM0"
    BAUDRATE = 250000
FILEPATH = "/uploads/"

class Dispatch():
    """Internal command and control class, passing data between printer, GUI, 
    and server.
    """
    def __init__(self, *args, **kwargs):
        self.logger = logger
        self.logger.info('args: %s', args)
        self.logger.info('kwargs: %s', kwargs)
        if 'baud' in args:
            self.logger.info( 'setting baud to: ' + kwargs[baud] )
            self.baud = kwargs[baud]
        else:
            self.baud = settings.BAUDRATE
        self.device = settings.DEVICE
        self.logger.info('BAUDRATE: %s', settings.BAUDRATE)
        self.logger.info('Port: %s', self.device)
        
        self.bot = None
        self.debug = False
        self.status = None
        self.path = os.path.dirname( os.path.abspath(__file__) ) 
        self.socket_passthrough_running = False
        self.connection = None
        self.control_connection = None
        
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Socket connection to server
        #self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        
        self.print_file = None
        self.print_record = None
        self.temperature = None
        self.temperature_logging_detailed = False
        self.usb = USBFiles()
        #self.startserver()
        self.server = None
        self.server_running = False
        self.commands = { "start passthrough" : self.start_socket_passthrough_thread,
                          "stop passthrough" : self.end_socket_passthrough_thread,
                          "send settings" : self.send_settings,
                          "gcode" : self.write_gcode_priorty,
                          "load print" : self.load_print,
                          "start print" : self.start_print,
                          "pause print" : self.stop_print,
                          "add_file" : self.add_print_to_db,
                        }
        self.watch_dog =  watch_dog.WatchDog()    
        self.watch_dog.connect()    
        self.watch_dog.light(self.watch_dog.light_mode)
        self.connect_bot()
        
    def startserver(self):
        """Starts Tornado web server located in server.py
        """
        if not self.server or self.server.poll() == 1 or self.server.poll() == -9: 
            self.logger.debug("starting server.py")
            
            self.path = os.path.dirname( os.path.abspath(__file__) ) 
            self.server = Popen(['python', self.path + '/server.py'])
            #stdoutdata, stderrdata = self.server.communicate()
            
            if self.server.poll() is None:
                self.server_running = True
                try:
                    #time.sleep(2.0)
                    #self.s.connect((HOST, PORT))
                    #self.s.setblocking(0)
                    #self.send_settings()
                    pass
                except socket.error, msg:
                    # probably got disconnected
                    self.logger.error('Socket ERROR: %s', msg)
            else:
                self.logger.debug('Startserver: failed server.poll')
        else:
            print "can't start server it seems to be running"
            self.logger.debug('Cant start server I think it is running: %s', self.server )
            self.logger.debug('Poll of server process: %s', self.server.poll() )

                
    def checkserver(self):
        """Check to see if web server is running
        """
        self.logger.info('checking server status:' )
        if self.server:
            if self.server.poll() == None:
                self.server_running = True
            else:
                self.server_running = False
        else:
            self.server_running = False
        return self.server_running

    def killserver(self):
        """Kill the web server
        """
        self.logger.info('killing server.py')
        if self.server:
            #self.s.shutdown(socket.SHUT_RDWR)
            #self.s.close()
            self.server.kill()
            self.server_running = False
            print self.server.poll()

    def send_settings(self):
        """Helper method to send printer .settings property to web server
        """
        settings = {"settings": self.bot.read_settings()}
        self.send_message_to_server(settings)
        
    def send_message_to_server(self, message):
        if message:
            if self.connection:
                try:
                    self.connection.sendall( json.dumps(message) + '\r\n' )
                except Exception, e:
                    self.logger.error('failed to send server message via socket: %s', e)
            else:
                self.logger.error('Server socket isnt connected')

    def start_server_listening_thread(self):
        self.logger.info('### Starting server listening thread')
        #time.sleep(2.0)
        self.s.bind(("127.0.0.1", 8000))
        self.s.listen(128)
        
        self.thread_read = threading.Thread(target=self.server_socket_control)
        self.thread_read.setDaemon(True)
        self.thread_read.setName('listen_to_server')
        self.thread_read.start()
                  
    def server_socket_control(self):
        self.logger.debug('Server listener thread Started')
        
        while 1:
            self.logger.debug('Waiting for connection')
            connection, address = self.s.accept()
            self.connection = connection
            self.logger.debug('Server Listener CONNECTED!!!')
        
            while self.bot.alive and self.server_running:
                self.logger.debug("self.bot.alive and self.server_running")
                message = None
                parameters = None
                try:
                    message = self.connection.recv(1024)
                except Exception, err:
                    self.logger.debug('socket message recv err: %s', err)
                if message:
                    self.logger.debug("got message from server")
                    try:
                        print message
                        data = json.loads(message)
                    except Exception, e:
                        print message
                        print e                
                    if "command" in data:
                        try:
                            command = data['command']
                            self.logger.debug("server message command, %s, command type, %s", command, type(command))
                            if 'parameters' in data:
                                self.logger.debug("server message parameters: %s", data['parameters'])
                                parameters = data['parameters']
                                self.commands[command](parameters)  # Run command
                            else:
                                self.commands[command]()  # Run command
                        except KeyError, e:
                            self.logger.error("unknown function request from server: %s", e)
                    else:
                        self.logger.error("unknown server message: %s", message)
                else:
                    pass 
                try:
                    while self.bot.count_messages() > 0:
                        mess = self.bot.read_message()
                        self.send_message_to_server(mess) 
                except (KeyboardInterrupt, SystemExit):
                    break                
            self.logger.error("bot seems to be dead")               
        
    def connect_bot(self, debug=False):
        self.bot = Marlin( self.baud, self.device, loud=False, debug=self.debug)
        #self.bot = Marlin( self.baud, loud=False, debug=self.debug)
        self.bot.connect()
        self.status = "Ready"
        
    def restart_bot(self):
        self.bot.disconnect()
        time.sleep(0.5)
        self.connect_bot()  

    def kill_bot(self):
        self.bot.disconnect()          
                
    def write_gcode(self, gcode):
        if gcode:
            print gcode
            if not self.bot.send_message(str(gcode)):
                self.logger.error("failed to send bot message")  
                
    def write_gcode_priorty(self, gcode):
        if gcode:
            print gcode
            if not self.bot.send_priorty_message(str(gcode)):
                self.logger.info("failed to send bot message, added to queue: %s", str(gcode))  

    def load_print(self, id):
        if self.bot.alive:
            if not self.bot.printing:
                self.logger.debug("file id passed to dispatcher is: %s", id) 
                try:
                    self.logger.debug("Trying to file with id %s in db", id) 
                    print_file = db.Files.get(db.Files.uuid == id)
                except db.Files.DoesNotExist, e:
                    self.logger.error("file with id: %s not in db", id)
                    print e
                try:
                    db_print_details = db.PrintFileDetails.get(db.PrintFileDetails.file_id == print_file.uuid)
                    print_details = db_print_details.dict()
                except db.PrintFileDetails.DoesNotExist, e:
                    print_details = self.process_gcode(print_file.file_path)
                    print_details['file_id'] = print_file.uuid
                    db_print_details = db.PrintFileDetails.create(**print_details)
                    
                self.logger.debug("file path: %s", print_file.file_path)
                
                self.bot.load_gcode(print_file.file_path)
                self.status = "file loaded"
                message = {"status" : self.status, "file": print_file.name, "length": len(self.bot.file_content)}
                self.send_message_to_server(message)
                self.print_file = print_file
                return print_details
                
    def add_print_to_db(self, file_path, location='USB', id = None):
        details = {}
        if not id:
            cname = str(uuid.uuid4())
        if location=='USB':
            details = self.usb.file_info(file_path)
            print details
        # Check if file is in db
        try:
            file_in_db = db.Files.get(db.Files.file_path == details['file_path'], 
                                      db.Files.file_mtime == details['file_mtime'],
                                      db.Files.file_size == details['file_size'],
                                    )
            self.logger.debug("Current file in db: %s", file_in_db.dict())

            details['uuid'] = file_in_db.uuid
        except db.Files.DoesNotExist, e:
            self.logger.info("Cant find file in DB: %s", e)
            details['uuid'] = cname
            file = db.Files.create(**details)
        return details
        
    def update_print(self, progress = 0.0, temperature=None):
        self.logger.debug("update print function: p %s, t %s", progress, temperature)
        self.print_record.progress = progress
        rc_count = self.print_record.save()
        self.logger.debug("%s, Print records updated", rc_count)
        if temperature:
            self.logger.debug("Print record id: %s", self.print_record.id)
            if self.temperature_logging_detailed:
                data_point = db.DataPoints.create(print_file = self.print_record.id,
                                                  datatype = 'temperature',
                                                  value = temperature )
            else:
                temperature_bin = round(float(temperature) * 2) / 2
                self.logger.debug("Temperature bin %s", temperature_bin)
                try:
                    data_point = db.DataPoints.get(db.DataPoints.print_file == self.print_record, 
                                      db.DataPoints.datatype == "temperature_bin",
                                      db.DataPoints.value == temperature_bin,
                                    )
                    data_point.value_count = data_point.value_count + 1
                    self.logger.debug("Temp bin %s, updated to count %s", temperature_bin, data_point.value_count )
                    data_point.save()
                except db.DataPoints.DoesNotExist, e:
                    self.logger.debug("New data point for temp bin %s", temperature_bin)
                    db.DataPoints.create(print_file = self.print_record, 
                                         datatype = "temperature_bin",
                                         value = temperature_bin,
                                         value_count = 1
                                    )

    def process_gcode(self, file_path):
        gcode_process = gcode.GCode(file_path=file_path)
        gcode_details = gcode_process.load()
        return gcode_details
        
                
    def start_print(self):
        self.logger.info("starting a print")
        if self.bot.alive:
            if not self.bot.printing:
                if self.bot.file_content:
                    self.print_record = db.Prints.create(print_file = self.print_file)
                    self.bot.start_printing()
                    self.status = "printing"
                    message = {"status" : self.status}
                    self.send_message_to_server(message) 
                else:
                    self.logger.error("can't start print no file content")
            else:
                self.logger.error("can't start print, bot currently printing")

        else:
            self.logger.error("can't start print bot is not alive")
           

    def stop_print(self):
        self.logger.info("pausing print")
        if self.bot.alive:
            if self.bot.printing:
                self.bot.stop_printing()
                self.status = "paused"
                message = {"status" : self.status}
                self.send_message_to_server(message) 
                
    def print_messages(self):
        while self.bot.count_messages() > 0:
            message = self.bot.read_message().strip()
            self.logger.info("bot says: %s", message)
                
    def start_socket_passthrough_thread(self):
        self.logger.info("starting socket pass through THREAD")
        self.bot.monitor_socket = self.s   #add web monitor to bot
        self.thread_read = threading.Thread(target=self.socket_passthrough)
        self.thread_read.setDaemon(True)
        self.thread_read.setName('read_socket')
        self.thread_read.start()
        self.status = "pass through started"
        message = {"status" : self.status}
        self.send_message_to_server(message) 
        
    def end_socket_passthrough_thread(self):
        self.bot.monitor_socket = None   #remove web monitor to bot
        self.bot.socket = None
        self.socket_passthrough_running = False
        self.status = "pass through ended"
        message = {"status" : self.status}
        self.send_message_to_server(message) 
        self.logger.info("ended socket pass through")

        
    def socket_passthrough(self, port=7777):
        self.logger.info("thread Started starting pass through on port 7777")
        self.socket_passthrough_running = True
        while self.bot.alive:
            self.logger.debug("bot is alive for starting pass through")
            
            self.srv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.srv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.srv.bind( ('', port) )
            self.srv.listen(1)

            while self.socket_passthrough_running:
            
                try:
                    self.control_connection, addr = self.srv.accept()
                    self.bot.socket = self.control_connection
                    self.logger.debug(self.control_connection)
                    self.logger.debug(addr)
                    while self.bot.alive and self.socket_passthrough_running:
                        for data in self.read_socketlines(self.control_connection):
                            self.logger.debug("<-socket read-> socket pass running: %s, %s", str(self.socket_passthrough_running), data)
                            if not self.socket_passthrough_running:
                                break
                            self.bot.send_message(data)
                            self.send_message_to_server({'incoming':data})
                        self.logger.debug("Socket Reset")
                        break
                except KeyboardInterrupt:
                   break
                except socket.error, msg:
                    self.logger.error("Socket Error:, %s Closing socket", msg)
                    break
            self.logger.debug("Socket Connected dropped" )              
            self.srv.close()
            break 
        self.socket_passthrough_running = False
        
    def read_socketlines(self, sock, recv_buffer=1024, delim='\n'):
        buffer = ''
        data = True
        while data:
            data = sock.recv(recv_buffer)
            buffer += data
            while buffer.find(delim) != -1:
                line, buffer = buffer.split(delim, 1)
                yield line
        return
        
    def update_interfaces(self, new_ssid, new_password):
        Interfacefile = "/etc/network/interfaces"
        ssid = "wpa-ssid"
        password = "wpa-psk"
        try:
            for line in fileinput.FileInput(Interfacefile,inplace=1):
                if ssid in line:
                    line = "    " + ssid + ' "' + new_ssid + '"\n'
                if password in line:
                    line = "    " + password + ' "' + new_password + '"\n'
                print line,
        except IOError, e:
            self.logger.error("failed to access inferfaces file: %s", e ) 
            
    def delete_print_file(self, id=None, name=None):
        if id:
            print_file = db.Files.get(db.Files.uuid == id)
            print print_file
            print dir(print_file)
            print_file.delete_instance() 
            file_path = self.path + '/' + FILEPATH + '/' + id
            self.logger.debug("deleting file: %s", file_path)
            try:
                os.remove(file_path)
            except Exception, err:
                self.logger.error("failed to delete file: %s, error: %s", file_path, err ) 

if __name__ == "__main__":
    print sys.argv
    dispatcher = Dispatch(sys.argv)
    dispatcher.startserver()
    dispatcher.send_message_to_server("hello world #2")
    time.sleep(2.5)
    #dispatcher.update_interfaces("emma", "1234Lily")
    #dispatcher.socket_passthrough()
    print "start server read"
    dispatcher.server_socket_control()
    print "after server socket control"
    for x in range(0, 10000):
        time.sleep(2.5)
        print dispatcher.bot.line_number
        try:
            #print "message count " + str(dispatcher.bot.count_messages())
            while dispatcher.bot.count_messages() > 0:
                mess = dispatcher.bot.read_message()
                #print "di: message from bot " + mess
                dispatcher.send_message_to_server(mess) 
        except (KeyboardInterrupt, SystemExit):
            break
    dispatcher.killserver()   
    print "exit"
      
  
