#!/usr/bin/python

# 
# 
# Write a line to the serial device and wait for an "ok" response.

# 
#   Installation on Ubuntu: sudo aptitude install python-serial
import sys
import os
import serial
import time
import threading
import re
from serial.tools import list_ports
import json
import logging

# create logger
logger = logging.getLogger(__name__)


class Marlin:
    def __init__(self, serial_buad, serial_port=None, loud=True, debug=False):
        """Marlin init documentation goes here..."""
        self.serial_port = serial_port
        self.serial_buad = serial_buad
        self.serial_timeout = 1
        self.outstanding_messages = 0
        self.messages = []
        self.priorty_command = []
        self.connected = False
        self.debug = debug
        self.loud = loud
        self.lock = threading.Lock()
        self.busy = False
        self.gcode_file = None
        self.gcode_file_name = None
        self.heating = False
        self.printing = False
        self.print_lines = 0
        self.temperature = []
        self.settings = {}
        self.socket = None
        self.monitor_socket = None
        self.status = None
        self.logger = logger
        self.alive = False
        self.temp_exp = re.compile("([TB@]\d*):([-+]?\d*\.?\d*)(?: ?\/)?([-+]?\d*\.?\d*)")

    def connect(self):
        """Create a connection to the serial port and launch the read thread
        """
        if self.debug:
            self.serial = serial.serial_for_url('loop://', timeout=self.serial_timeout)
        else:
            self.serial = serial.Serial()
            if not self.serial_port:
                ports = list(self.serial_ports())
                self.logger.info("Ports: %s", ports) 
                if len(ports) > 1:
                    self.logger.info("Ports: %s", ports) 
                    if '/dev/ttyACM0' in ports:
                        self.serial.port = '/dev/ttyACM0'
                else:
                    self.logger.info("Ports: %s", ports) 
                    self.serial.port = ports[0]
            else:        
                self.serial.port = self.serial_port
            self.serial.baudrate = self.serial_buad
            self.serial.timeout = self.serial_timeout
            try:
                self.serial.open()
            except serial.SerialException, e:
                sys.stderr.write("Could not open serial port %s: %s\n" % (self.serial.portstr, e))
                self.logger.info("running port search")  
                print(list(self.serial_ports()))
                sys.exit(1)
            except OSError, e:
                self.logger.info("serial error, checking serial ports") 
                self.logger.info("Ports: %s", list(self.serial_ports()))
                
        self.alive = True
        self.thread_read = threading.Thread(target=self.reader)
        self.thread_read.setDaemon(True)
        self.thread_read.setName('read_serial')
        self.thread_read.start()
            
    def disconnect(self): 
        """Stop copying"""
        if self.alive:
            self.logger.info("disconnecting...") 
            
            self.alive = False
            self.connected = False
            self.serial.close()
            del self.messages[:]
            self.thread_read.join()

    def reader(self):
        """Serial reading thread, loop forever and read serial"""
        if self.debug:
            self.send_message("M501")
        while self.alive:
            try:
                data = self.serial.readline()              # read one, blocking
                if data:
                    #self.logger.debug("in: %s", data.strip()) 
                    with self.lock:
                        if self.socket:
                            self.socket.sendall( data )
                            if self.monitor_socket:
                                try:
                                    self.monitor_socket.sendall( json.dumps({"outgoing" : data.rstrip()}) + '\r\n')
                                except:
                                    pass  # fix me
                            #break
                        if (data[:2] == "ok"):
                            if self.busy:
                                self.busy = False 
                                self.send_priorty_message()
                            if (data[:4] == "ok T"):
                                self.parse_temperature(data)
                        elif data[:2] == "T:":
                            self.parse_temperature(data)
                        elif (data[:5] == "echo:"):
                            if (data[5:6] == "M"):
                                self.parse_settings(data[5:])
                            self.messages.append(data)     # Add message to list 
                        elif ( self.connected == False and data[:5] == "start"):
                            self.connected = True
                            del self.messages[:]               #empty messages
                            self.busy = False
                            if self.loud:
                                self.logger.info("CONNENTED") 
                        else:
                            if self.loud or self.connected == False:
                                self.logger.info(data)
                            if self.connected == True:
                                self.messages.append(data)     # Add message to list   
            except serial.SerialException:
                self.logger.info("serial exception") 
                self.alive = False
        self.alive = False
        
    def count_messages(self):
        """ Count messages """
        with self.lock:   
            return len(self.messages)
        
    def read_message(self):
        """ Report message """
        with self.lock:
            if len(self.messages) > 0:
                message = self.messages[0] 
                self.messages.pop(0)
                if self.loud:
                    self.logger.info(message) 
                return str(message)
    
    def send_message(self,gcode):
        """Send a single gcode command to serial port or return false if busy
        """
        
        if self.alive:
            with self.lock:
                if self.debug:
                    self.busy = False
                    self._debugSEND(gcode)
                    return True
                if self.busy:
                    return False
                #try:
                if self.loud: 
                    self.logger.info("message: %s ", gcode.strip())
                #gcode = str(gcode)
                self.serial.write(gcode.strip() + "\n") 
                self.busy = True
                #except serial.SerialException, e:
                #    print e
                return True
                
    def send_no_wait(self,gcode):
        """ Send a command to serial port even if the printer is busy"""
        if self.alive:
            with self.lock:
                if self.loud:
                    self.logger.info("message: %s ", gcode.strip())
                self.serial.write(gcode.strip() + "\n") 
                return True

    def clear_busy(self):
        with self.lock:
            self.busy = False

    def send_priorty_message(self, gcode =  None):
        """ 
        Send a priorty message.
        """
        if self.alive:
            if gcode:
                if self.loud:
                    self.logger.info("adding to list: %s ", gcode.strip()) 
                self.priorty_command.append(gcode.strip() + "\n")
            if self.busy:
                return False
            try:
                if len(self.priorty_command) >0:
                    message = self.priorty_command[0]
                    self.priorty_command.pop(0)
                    self.serial.write(message) 
                    self.busy = True
                    return True
                return False
            except serial.SerialException, e:
                sys.stderr.write("Could not open serial port %s: %s\n" % (self.serial.portstr, e))
     
    def load_gcode(self, GcodeFile):
        """ Load a file into list to be sent into the serial link
        """
        self.file_content = []
        self.line_number = 0
        try:
            self.gcode_file = open(GcodeFile)
            self.gcode_file_name = GcodeFile
            with self.gcode_file as f:
                for line in f:
                    if self.gcode_fixer(line):
                        self.file_content.append(self.gcode_fixer(line))
            self.print_lines = len(self.file_content)
            self.logger.info("%s, lines loaded", self.print_lines)
        except IOError, e:
            self.logger.error("error %s", e)  
            self.logger.error('failed to open file: %s', GcodeFile) 

    def start_printing(self):
        """ Start feeding thread 
        """
        self.printing = True
        self.thread_print = threading.Thread(target=self._printing)
        self.thread_print.setDaemon(True)
        self.thread_print.setName('printer feeder')
        self.thread_print.start()

    def _printing(self):
        """Actual print thread which feeds list into serial port
        """
        try:
            while self.line_number < len(self.file_content):
                gcode = self.file_content[self.line_number]
                with self.lock:
                    if not self.printing:
                        break
                while not self.send_message(gcode): 
                        #print self.read_message()
                        #print self.busy
                    pass
                self.line_number += 1
        except ValueError, e:
            self.logger.error("error printing %s", e)  
            self.printing = False
        if self.line_number >= len(self.file_content):
            self.end_print()

    def stop_printing(self):
        """ Stop feeding thread by setting self.printing to False
        """
        self.logger.info("pausing at %s ", self.line_number)
        with self.lock:
            self.printing = False
            
    def end_print(self):
        """ Finish Up print and clear everything
        """
        self.logger.info("ending print")
        with self.lock:
            self.printing = False
            self.file_content = None
            self.line_number = None
            self.gcode_file = None
            self.self.gcode_file_name = None
            self.print_lines = 0
    
    def gcode_fixer(self, gcode):
        """Simple gcode line conditioner. Currently just removes comments
        """
        comment = ";"
        fixed = gcode.strip().split(comment,1)[0]
        return fixed
            
    def parse_temperature(self, temperature):
        """Parse the temperature report from the bot """
        if "W:" in temperature:
            self.logger.info("heating...")
            self.heating = True
        else:
            self.heating = False
        temperature = self.temp_exp.findall(temperature)
        output = {}
        keys = ["Extruder", "Extruder 1", "Extruder 2", "Bed"]
        subkeys = ["temperature", "setpoint", "power"]
        mapvalues = ["T","T1","T2","B"]
        for key in keys:
            output[key] = {}
            for subkey in subkeys:
                output[key][subkey] = ''
        count_setpoint = 0
        for temp in temperature:
            if temp[0] == '@':
                if not count_setpoint:
                    output[keys[0]][subkeys[2]] = temp[1]
                    count_setpoint == 1
                else:
                    output[keys[3]][subkeys[2]] = temp[1]
            for idx, mapvalue in enumerate(mapvalues):
                if temp[0] == mapvalue:
                    output[keys[idx]][subkeys[0]] = temp[1]
                    output[keys[idx]][subkeys[1]] = temp[2]
        self.temperature = output
        #self.logger.debug(self.temperature)
            
    def get_temperature(self, repeat=False):
        """Thread safe reading of self.temperature include a repeat option
        to resend M105 to ask for a temperature update
        """
        with self.lock:
            if repeat:
                self.send_priorty_message("M105")
            return self.temperature
        
    def parse_settings(self, data):
        """Parse the Settings report from the bot """
        output = {}
        pieces = data.split()
        setting = pieces[0].strip()
        output = {}
        for piece in pieces:
            self.logger.debug(piece)
            if piece[:1] == ";":
                break
            if piece[:1] in ["X", "Y", "Z", "E", "S", "P", "I", "K"]:
                output[piece[:1]] = piece[1:]
        if setting not in self.settings:
            self.settings[setting] = {}
        for key in output:
            self.settings[setting][key] = output[key]
        self.logger.debug(data)
            
    def read_settings(self):
        """Thread safe way to return settings
        """
        with self.lock:
            return self.settings
        
    def serial_ports(self):
        """
        Returns a generator for all available serial ports
        """
        if os.name == 'nt':
            # windows
            for i in range(256):
                try:
                    s = serial.Serial(i)
                    s.close()
                    yield 'COM' + str(i + 1)
                except serial.SerialException:
                    pass
        else:
            # unix
            for port in list_ports.comports():
                yield port[0]

    def _debugSEND(self, gcode):
        """Fake data to send during development
        
        """
        time.sleep(.5)
        if gcode[:4] == "M105":
            self.serial.write("ok T:24.1/0.0 B:25.1/0.0 T1:24.2/0.0 @:0 B@:0")
        elif gcode[:1] == "M":
            message = ("echo:M92 X118.52 ; calibrate X\n"
                "echo:M92 Y118.52 ; calibrate Y\n"
                "echo:M92 Z4031.50 ; calibrate Z\n"
                "echo:M92 E1850 ; calibrate E\n"
                "echo:M220 S100\n"
                "echo:M221 S100\n"
                "echo:M201 X1500 Y1500\n")
            self.serial.write(message)
        else:
            self.serial.write("ok\n")