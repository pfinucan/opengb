//
// Licensed under the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.                                                        

$(document).ready(function() {
    if (!window.console) window.console = {};
    if (!window.console.log) window.console.log = function() {};

    $(".file_button, .command_button").click(function() {
        if ( ! $( this ).hasClass( "disabled" ) ) {
            process_button($(this));
        } 
        return false;
    });
    
    updater.start();
});


function print_command(button) {
    console.log ( "start print" );

        var message = {};
        message["command"] = "start print";
        updater.socket.send(JSON.stringify(message));
   
    $("#print").addClass("disabled")

}

function process_button(button) {
    console.log ( button.text() );
    console.log ( button );

    var message = {};
    
    if ( button.hasClass( "file_button" ) ){
        message["command"] = "load print";
        message["parameters"] = button.text();
    } else if ( button.hasClass( "command_button" ) ){
        switch ( button.attr('id') ){
            case "print":
                message["command"] = "start print";
                break;
            case "pause":
                message["command"] = "pause print";
                break;
        }
    }
    updater.socket.send(JSON.stringify(message));
}

function process_status(message) {
    console.log ( "updating status" );
    console.log ( message.status );
    
    switch (message.status ){
        case "Initializing...":
            break;
        case "printing":
            $("#print").addClass("disabled")
            $("#pause").removeClass("disabled")
            break;
        case "paused":
            $("#print").removeClass("disabled")
            $("#pause").addClass("disabled")
            break;
        case "file loaded":
            $(".file_button").addClass("disabled")
            $("#print").removeClass("disabled")
            break;
        default:
            
    }
    $("#status").text(message.status);
    $("#status").closest( "p" ).effect("highlight", {}, 3000);
    
}

var updater = {
    socket: null,

    start: function() {
        var url = "ws://" + location.host + "/chatsocket";
        updater.socket = new WebSocket(url);
        updater.socket.onmessage = function(event) {
            console.log ( event.data );
            updater.showMessage(JSON.parse(event.data));
        }
    },

    showMessage: function(message) {

        if (message.status) {
             console.log ( message.status );
            process_status(message);
        }
    }
};
