import cv2.cv as cv

capture = cv.CaptureFromCAM(0)
cv.SetCaptureProperty(capture,cv.CV_CAP_PROP_FRAME_WIDTH, 1280)
cv.SetCaptureProperty(capture,cv.CV_CAP_PROP_FRAME_HEIGHT, 960)

while True:
    img = cv.QueryFrame(capture)
    break
    
file = "test7.png"
cv.SaveImage(file, img)
