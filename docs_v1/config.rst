config package
==================

Submodules
----------

config.settings module
--------------------------

.. automodule:: config.settings
    :members:
    :undoc-members:
    :show-inheritance:

config.style module
-----------------------

.. automodule:: config.style
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: config
    :members:
    :undoc-members:
    :show-inheritance:
