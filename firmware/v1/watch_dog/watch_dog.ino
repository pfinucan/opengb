#include "Arduino.h"
#include <TimerOne.h>
#include <DistanceGP2Y0A21YK.h>

// Set the time delay. Google Arduino Timer1

/*  Timer Setting
 *
 *  For 16MHz:
 *  Prescale Time_Per_Tick Max_Period
 *  1        0.0625 uS     8.192 mS
 *  8        0.5 uS        65.536 ms
 *  64       4 uS          524.288 ms
 *  256      16 uS         2097.152 ms
 *  1024     64 us         8388.608 ms
 *
 *  The interupt fires when the timer hits the max period
 */ 

/* Variables */
int temperature=0;
long TimerIncrements=0;
long  timerPeriod=100000;
long timerOffset=1;
bool comment=0;

volatile boolean isProx_Sensing=true;
volatile boolean isRBG_temperature=false;
volatile boolean isFilament_Measurement=true;
volatile boolean isControling_light=false;
volatile boolean isBusy=false;
volatile boolean isDebugging=true;

/* Serial Connection (USB) variables */
int baud = 9600;		// connection rate
const int MAX_CMD_SIZE = 256;
char buffer[MAX_CMD_SIZE];	// incoming serial data (array)
char inChar;			// single diget 
int index=0;			// serial data index (points to array position)
//
char serial_char; // value for each byte read in from serial comms
int serial_count = 0; // current length of command
char *strchr_pointer; // just a pointer to find chars in the cmd string like X, Y, Z, E, etc

int white_light = 5;   // pin for white light
int red = 9;
int green = 3;
int blue = 6;
int brightness = 255;    // how bright the LED is
int fadeAmount = 10;    // how many points to fade the LED by
int setpoint_brightness = 0; // current light setting
volatile boolean light_latched=false;
int proxy_distance = 20;
long delay_until = 0;
long delay_lenght = 10000;
int light_trigger_count = 0;

int red_value = 0;
int green_value = 0;
int blue_value = 0;
int white_value = 0;

int red_set = 0;
int green_set = 0;
int blue_set = 0;
int white_set = 0; 

// encoder
static unsigned long last_interrupt_time = 0;
 int val; 
 int encoder0PinA = 2;
 int encoder0PinB = 4;
 int encoder0Pos = 0;
 int encoder0PosLast = 0;
 int encoder0PinALast = LOW;
 int n = LOW;
 
static boolean A=false;
static boolean B=false;
static boolean C=false;
static boolean D=false;

DistanceGP2Y0A21YK Dist;
int distance;


void setup()
{
  Serial.begin(baud);
  Dist.begin(3);
  
  Serial.println("start"); 

  // Define pins
  pinMode(white_light, OUTPUT); 
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(blue, OUTPUT); 
  
  pinMode(encoder0PinA, INPUT); 
  digitalWrite(encoder0PinA, HIGH);       // turn on pullup resistor
  pinMode(encoder0PinB, INPUT); 
  digitalWrite(encoder0PinB, HIGH);       // turn on pullup resistor
  attachInterrupt(0, doEncoder, CHANGE);  // encoder pin on interrupt 0 - pin 2
  
  /* Attach runInterrupt function to Timer1 */
 // Timer1.initialize(timerPeriod); // Timer for updating pwm pins
 // Timer1.attachInterrupt(runInterrupt);	
  
  
  A = digitalRead(encoder0PinA);
  B = digitalRead(encoder0PinB);
}

void loop()
{
  Read_Serial_Connection(); 

   if(isProx_Sensing)
   {
      proxy_sensor();
   }

   if(isFilament_Measurement)
   {
     
    if(encoder0Pos != encoder0PosLast)
    {
      Serial.print("echo:"); 
      Serial.println(encoder0Pos); 
      encoder0PosLast = encoder0Pos;
    }
   }
   
   run_fade(white_set, &white_value);
   run_fade(blue_set, &blue_value);
   run_fade(red_set, &red_value);
   run_fade(green_set, &green_value);
   
   analogWrite(blue, blue_value);
   analogWrite(red, red_value);
   analogWrite(green, green_value);
   
   analogWrite(white_light, white_value);   
   
   delay(50);
   
}

/* Read the usb serial connection */
void Read_Serial_Connection()
{
	{
	 /* All done with moving get next command */
         /* if (!isBusy && Serial.available() > 0) */
	  if (Serial.available() > 0)
	  {
	    /* Read a character */
	    inChar = Serial.read();
	    
	    /* Detect end of command. Time to process command and clear 
	    the buffer and reset comment mode flag */
	    if (inChar == '\n' || inChar == '\r') 
	    { 
	    	    if(isDebugging)
                            Serial.print("echo:"); 
	    	    	    Serial.println(buffer);
	      
	      buffer[index]=0;
	      parse_Command(buffer, index);
	      index = 0;
	      comment = false; 
	    }
	    
	    /* still in middle of reading the command */
	    else 
	    {
	      /* check for start of command */
	      if (inChar == ';' || inChar == '(')
	      {
		comment = true;
	      }
	      
	      if (comment != true) // ignore if a comment has started
	      {
		buffer[index] = inChar; // add byte to buffer string
		index++;
		if (index > MAX_CMD_SIZE) // overflow, dump and restart
		{
		  index = 0;
		  Serial.flush();
		}
	      }
	    }
	  }
	}
}

/* Parse the Command 
   S - Used to set common function settings
   L - Used to adjust light setting



*/

void parse_Command(char command[], int command_length)
{
  /* Return the number starting at the 2 digit (aka 1) and ending at a space*/
  int cmd = (int)strtod(&command[1], NULL);

  if ( command_length > 0 )
  {
    switch (command[0]) { 
     case 'S':
      switch (cmd) { 
        case 1:
          isProx_Sensing = true;
          light_latched = false;
        break;
        case 2:
          isProx_Sensing = false;
          light_latched = true;
        break;
        case 3:
          isRBG_temperature = true;
        break;
        case 4:
          isRBG_temperature = false;
        break;
      }
      
      getInt('fade', command, false, &fadeAmount);

      break;
      
     case 'L':
       brightness = map(cmd, 0, 100, 0, 255);
       getInt('T', command, false, &temperature);
       
       if(isRBG_temperature)
       {
         red_set = map(temperature, 0, 100, 0, brightness);
         blue_set = brightness - map(temperature, 0, 100, 0, brightness);
         green_set = 0; 
       }
       else
       {
         getInt('R', command, true, &red_set);
         getInt('G', command, true, &green_set);
         getInt('B', command, true, &blue_set);
       }

      break;
    }
  }
 
  /* done processing commands */
  if (Serial.available() <= 0) {
    Serial.print("echo:");
    Serial.println(command);
    Serial.print("echo: brightness:");
    Serial.println(brightness);
    Serial.print("echo: Temp:");
    Serial.println(temperature);
    Serial.print("echo: R:");
    Serial.println(red_value);
    Serial.print("echo: G:");
    Serial.println(green_value);
    Serial.print("echo: B:");
    Serial.println(blue_value);
    Serial.print("echo: latch:");
    Serial.println(light_latched);
    Serial.print("echo: isProx_Sensing:");
    Serial.println(isProx_Sensing);
    Serial.print("echo: isRBG_temperature:");
    Serial.println(isRBG_temperature);
    
  }
  
}

/* Parse the individual gcode commands */
boolean getValue(char key, char command[], double* value)
{  
	strchr_pointer = strchr(buffer, key);
	if (strchr_pointer != NULL) // We found a key value
	{
	  *value = (double)strtod(&command[strchr_pointer - command + 1], NULL);
	  return true;  
	}
	return false;
}

/* Parse the individual gcode commands */
boolean getInt(char key, char command[], bool pwm_map, int* value)
{  
	strchr_pointer = strchr(buffer, key);
	if (strchr_pointer != NULL) // We found a key value
	{
	  int temp = (int)strtod(&command[strchr_pointer - command + 1], NULL);
          if( pwm_map )
          {
             *value = map(temp, 0, 100, 0, brightness);
	  } else {
             *value = temp;
          }

          return true;  
	}
	return false;
}

void proxy_sensor()
{
     if(light_trigger_count == 5)
     {
         light_trigger_count = 0;
     }
     
     distance = Dist.getDistanceCentimeter();
       
     if(distance < proxy_distance)  // Trigger
     {
       light_trigger_count = light_trigger_count + 1;

         if(light_trigger_count == 4)
         {
           
           Serial.print("echo: Distance in centimers: ");
           Serial.print(distance);  
           Serial.print(", Trigger: ");
           Serial.println(light_trigger_count);  
           
           delay_until = millis() + delay_lenght;
           white_set = brightness;
           
           if(isRBG_temperature)
           {
             // percent of blue to red
             red_set = map(temperature, 0, 100, 0, brightness);
             blue_set = brightness - map(temperature, 0, 100, 0, brightness);
             green_set = 0; 
             
           } else {
             red_set = brightness; 
             green_set = brightness; 
             blue_set = brightness;
                 Serial.print("echo: setting RGB to:");
                 Serial.println(brightness);
           }
         }
         
       } else {
         light_trigger_count = 0;
     }
     
     if( delay_until < millis() && !light_latched)
     {
         white_set = 0;
         red_set = 0; 
         green_set = 0; 
         blue_set = 0;
     }
     
     
}


void run_fade(int setpoint_b, int* current_b)
{
    if(*current_b != setpoint_b) {
       if(*current_b < setpoint_b)
       {
         *current_b = *current_b + fadeAmount;
         if(*current_b > setpoint_b)
         {
            *current_b = setpoint_b;
         }
     } else {
         *current_b = *current_b - fadeAmount;
         if(*current_b < 0)
         {
            *current_b = 0; 
         }
     }
    }
}

void doEncoder() {
  /* If pinA and pinB are both high or both low, it is spinning
   * forward. If they're different, it's going backward.
   *
   * For more information on speeding up this process, see
   * [Reference/PortManipulation], specifically the PIND register.
   */

  unsigned long interrupt_time = millis();
  if (interrupt_time - last_interrupt_time > 25)
  {
    C = digitalRead(encoder0PinA);
    D = digitalRead(encoder0PinB);
   if(  A == C && B == D)
    {
      // no change ignore
    } else { 
      if (C == D) {
        encoder0Pos++;
      } else {
        encoder0Pos--;
      } 
      A = C;
      B = D;
    last_interrupt_time = interrupt_time;
   }
  }
}
