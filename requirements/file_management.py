import os
import sys

class USBFiles(object):
    """
    """
    def __init__(self, root_path="/media"):
        self.walk_dir = root_path
        self.file_list = []
        
    def files(self):

        for root, subdirs, files in os.walk(self.walk_dir):
            for subdir in subdirs:
                #print('\t- subdirectory ' + subdir)
                pass

            for filename in files:
                file_path = os.path.join(root, filename)
                file_details = self.file_info(file_path)
                if file_details:
                    self.file_list.append(file_details)
                    
        return self.file_list
        
    def file_info(self, file_path):
        fileExtension = os.path.splitext(file_path)[1]
        #print fileExtension
        if fileExtension in ['.gcode', '.gco', '.GCODE', '.GCO']:
            file_details = {
                    'name': os.path.basename(file_path), 
                    'file_location': "USB",
                    'file_path': file_path,
                    'file_mtime': os.path.getmtime(file_path),
                    'file_size': os.path.getsize(file_path),
                    }
                    
            #print('\t- file %s (full path: %s)' % (filename, file_path))
            return file_details
        else:
            return None
            
            
        
